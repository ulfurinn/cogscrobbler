#ifndef SIDEBARITEM_H
#define SIDEBARITEM_H

#include <QObject>
#include <functional>

class SidebarItem : public QObject {
  Q_OBJECT
public:
  explicit SidebarItem( QString title );
  const QString &title();
  SidebarItem *child( int );
  SidebarItem *parent();
  void setParent( SidebarItem * );
  int row();
  unsigned int size();

  void append( SidebarItem *child );

  template <typename T> void setAction( T f ) { action_ = f; }
  void activate();

signals:

public slots:

private:
  std::vector<SidebarItem *> children_;
  SidebarItem *parent_;
  QString title_;

  int indexOf( SidebarItem *child );

  std::function<void()> action_;
};

#endif // SIDEBARITEM_H
