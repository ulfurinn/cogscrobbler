#ifndef LASTFM_H
#define LASTFM_H

#include <QString>
#include <QTimer>
#include <boost/signals2.hpp>

class LastFM {
public:
  struct ScrobbleItem {
    QString artist;
    QString release;
    QString track;
    int startTimestamp;
    int duration;
  };

  explicit LastFM();

  void verify();
  const QString &token();
  void scrobble( const std::vector<ScrobbleItem> & );
  std::vector<ScrobbleItem> batch( int );
  void scrobbled( bool ok );

  const QString &sessionKey();

  void authFirstStepPassed( const QString &token );
  void verified( const QString &username );

  boost::signals2::signal<void()> unauthorized;
  boost::signals2::signal<void( const QString &token )> authSecondStep;
  boost::signals2::signal<void( const QString &key, const QString &username )> authorized;

  void setCredentials( const QString &key );

  void initiateAuth();
  void completeAuth();

private:
  QString sessionKey_;
  QString token_;
  QString username_;

  std::vector<ScrobbleItem> scrobbleQueue_;
  std::vector<ScrobbleItem> pendingScrobble_;

  QTimer timer_;

  void scrobbleBatch();
};

#endif // LASTFM_H
