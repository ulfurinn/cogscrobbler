#ifndef HTTP_REQUEST_H
#define HTTP_REQUEST_H

#include <QNetworkReply>
#include <QObject>

class HTTPRequest : public QObject {
  Q_OBJECT
public:
  explicit HTTPRequest();

  virtual QNetworkReply *startRequest() = 0;
  virtual void handleResponse() = 0;
  virtual void handleError( QNetworkReply::NetworkError ) = 0;

  void execute();

signals:
  void aborted();

public slots:

protected:
  QNetworkReply *reply_;

private slots:
  void error( QNetworkReply::NetworkError );
  void finished();

public:
  static QNetworkAccessManager *manager;
};

#endif // HTTP_REQUEST_H
