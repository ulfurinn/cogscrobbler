#ifndef EXTERNALBROWSERPAGE_H
#define EXTERNALBROWSERPAGE_H

#include <QWebEnginePage>

class ExternalBrowserPage : public QWebEnginePage {
  Q_OBJECT
public:
  ExternalBrowserPage( QObject *parent = nullptr );

  // QWebEnginePage interface
protected:
  virtual bool acceptNavigationRequest( const QUrl &url, NavigationType type, bool isMainFrame ) override;
};

#endif // EXTERNALBROWSERPAGE_H
