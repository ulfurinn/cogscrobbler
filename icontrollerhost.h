#ifndef IFRONTVIEW
#define IFRONTVIEW

#include "base_controller.h"
#include "enum.h"
#include "istorage.h"
#include <vector>

class LastFM;
namespace Discogs {
class Provider;
}
namespace Model {
struct Track;
}

class IControllerHost {
public:
  virtual void activateController( BaseController * ) = 0;
  virtual void scrobble( const std::vector<Model::Track> &list, ScrobbleTime st, unsigned int listen ) = 0;
  virtual Discogs::Provider *discogs() = 0;
  virtual IStorage *storage() = 0;
};

#endif // IFRONTVIEW
