#ifndef LOCALCOLLECTION_LOCAL_COLLECTION_VIEW_H
#define LOCALCOLLECTION_LOCAL_COLLECTION_VIEW_H

#include <QWidget>
#include <boost/signals2.hpp>

#include "models.h"

namespace LocalCollection {

namespace Ui {
class View;
}

using namespace boost::signals2;
class View : public QWidget {
  Q_OBJECT

public:
  explicit View( QWidget *parent = 0 );
  ~View();

  void addRelease( Model::Release * );

  signal<void( Model::Release * )> releaseClicked;

private:
  Ui::View *ui;
  QWidget *empty_;

  void createDummy();
};

} // namespace LocalCollection
#endif // LOCALCOLLECTION_LOCAL_COLLECTION_VIEW_H
