#include "clickablegraphicsview.h"

ClickableGraphicsView::ClickableGraphicsView( QWidget *parent ) : QGraphicsView( parent ) {}

void ClickableGraphicsView::mousePressEvent( QMouseEvent *me ) {
  if ( me->buttons() ^ me->button() ) {
    QGraphicsView::mousePressEvent( me );
    return;
  }

  if ( me->button() ) {
    emit( clicked() );
    me->accept();
    return;
  }

  QGraphicsView::mousePressEvent( me );
}
