#ifndef BASECONTROLLER_H
#define BASECONTROLLER_H

#include "istorage.h"

class QWidget;
class IControllerHost;

class BaseController {
public:
  BaseController();
  virtual ~BaseController();
  virtual void setStorage( IStorage * );
  virtual void execute( IControllerHost * ) = 0;
  virtual QWidget *view() = 0;

protected:
  IControllerHost *controllerHost_;
  IStorage *storage_;
};

#endif // BASECONTROLLER_H
