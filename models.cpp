#include "models.h"

void Model::Release::apply( Model::Releasable::Visitor f ) { f( this ); }

void Model::Master::apply( Model::Releasable::Visitor f ) { f( this ); }
