#ifndef ISTORAGE_H
#define ISTORAGE_H

#include <vector>

namespace Model {
struct Release;
}

class IStorage {
public:
  virtual std::vector<Model::Release *> fetch() = 0;
  virtual Model::Release *save( Model::Release * ) = 0;
};

#endif // ISTORAGE_H
