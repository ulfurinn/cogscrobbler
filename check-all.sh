#!/bin/sh

find . -type f \( -iname \*.cpp -o -iname \*.h \) -print0 | xargs -0 -n1 ./check-format.sh

