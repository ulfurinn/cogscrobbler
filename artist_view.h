#ifndef VIEW_VIEW_ARTIST_H
#define VIEW_VIEW_ARTIST_H

#include <boost/signals2.hpp>

#include "models.h"
#include <QFrame>

namespace Artist {

class Ui_View;

class View : public QFrame {
  Q_OBJECT

public:
  explicit View( QWidget *parent = 0 );
  ~View();

  void setName( const QString & );
  void setBio( const QString & );

  void addRelease( Model::Master * );
  void addRelease( Model::Release * );

  boost::signals2::signal<void( Model::Master * )> masterClicked;
  boost::signals2::signal<void( Model::Release * )> releaseClicked;

private:
  Ui_View *ui;
  QWidget *empty_;

  void createDummy();
};

} // namespace View
#endif // VIEW_VIEW_ARTIST_H
