#ifndef RELEASE_RELEASE_VIEW_H
#define RELEASE_RELEASE_VIEW_H

#include "enum.h"
#include "models.h"
#include <QWidget>
#include <boost/signals2.hpp>

class QAction;

namespace Release {

namespace Ui {
class View;
}

using namespace boost::signals2;
class View : public QWidget {
  Q_OBJECT

public:
  explicit View( QWidget *parent = 0 );
  ~View();

  void setArtist( const QString & );
  void setTitle( const QString & );
  void setDate( int );
  void setTracks( std::vector<Model::Track> );

  signal<void( ScrobbleTime, QDateTime )> scrobble;
  signal<void( int, bool )> trackSelectionChanged;
  signal<void( int, int )> trackDurationChanged;
  signal<void( bool )> massSelect;
  signal<void()> saveLocal;
  signal<void()> saveDiscogs;

private:
  Ui::View *ui;
  QWidget *empty_;
  QString artist_;

  QAction *saveLocal_;
  QAction *saveDiscogs_;

  void createDummy();
};

} // namespace Release
#endif // RELEASE_RELEASE_VIEW_H
