#ifndef RELEASE_BUTTON_H
#define RELEASE_BUTTON_H

#include "models.h"
#include <QFrame>

namespace Ui {
class ReleaseButton;
}

class ReleaseButton : public QFrame {
  Q_OBJECT

public:
  explicit ReleaseButton( QWidget *parent = 0 );
  ~ReleaseButton();

  void showArtist( bool );
  void setRelease( Model::Master * );
  void setRelease( Model::Release * );

signals:
  void clicked();

protected:
  virtual void mousePressEvent( QMouseEvent * );

private:
  Ui::ReleaseButton *ui;
};

#endif // RELEASE_BUTTON_H
