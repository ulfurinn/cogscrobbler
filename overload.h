#ifndef OVERLOAD_H
#define OVERLOAD_H

template <class... Fs> struct overload_set_;

template <class F1, class... Fs> struct overload_set_<F1, Fs...> : F1, overload_set_<Fs...> {
  overload_set_( F1 head, Fs... tail ) : F1( head ), overload_set_<Fs...>( tail... ) {}

  using F1::operator();
  using overload_set_<Fs...>::operator();
};

template <class F> struct overload_set_<F> : F {
  overload_set_( F f ) : F( f ) {}
  using F::operator();
};

template <class... Fs> overload_set_<Fs...> overload( Fs... x ) { return overload_set_<Fs...>( x... ); }

#endif // OVERLOAD_H
