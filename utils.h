#ifndef UTILS_H
#define UTILS_H

#include <QString>

QString randomString( int length );
QString md5( const QString & );
int parseDuration( const QString & );
QString stripDiscogsIndex( QString );

template <typename Iterator, typename Function> void for_each_indexed( Iterator begin, Iterator end, Function f ) {
  Iterator first = begin;
  for ( ; begin != end; begin++ ) {
    f( begin - first, *begin );
  }
}

#endif // UTILS_H
