#ifndef SEARCH_H
#define SEARCH_H

#include "base_controller.h"
#include "models.h"
#include <QObject>
#include <functional>

namespace Discogs {
class Provider;
}

namespace Search {

class View;

class Controller : public BaseController {
public:
  explicit Controller( QString term );
  ~Controller();
  virtual QWidget *view() override;
  virtual void execute( IControllerHost * ) override;

signals:

public slots:

private:
  QString term_;
  View *view_;
};
}

#endif // SEARCH_H
