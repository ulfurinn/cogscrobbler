#include "discogs_provider.h"
#include "discogs_backend.h"
#include "utils.h"
#include <QDebug>
#include <QJsonArray>
#include <QJsonObject>
#include <algorithm>
#include <numeric>

namespace Discogs {

class Cache {
public:
  std::map<int, Model::Artist *> artists_;
  std::map<int, std::vector<Model::Releasable *>> artistReleases_;
  std::map<int, Model::Release *> releases_;
  std::map<int, bool> releaseDetailsLoaded_;
  std::map<int, Model::Master *> masters_;
  std::map<int, bool> masterDetailsLoaded_;
  std::map<int, std::map<int, Model::MasterVersion *>> masterVersions_;
};

Provider::Provider( Backend *backend ) : backend_( backend ), cache_( new Cache() ) {}

Provider::~Provider() {}

void Provider::artistSearch( const QString &term, Callback::ArtistList cb ) {
  backend_
      ->artistSearch( term,
                      [cb]( const QJsonDocument &doc ) {
                        std::vector<Model::Artist *> parsed;
                        auto artists = doc.object()["results"].toArray();
                        auto filtered =
                            std::remove_if( artists.begin(), artists.end(), []( const QJsonValue &v ) { return v.toObject()["type"].toString() != "artist"; } );
                        std::transform( artists.begin(), filtered, std::back_inserter( parsed ), []( const QJsonValue &v ) {
                          auto o = v.toObject();
                          auto artist = new Model::Artist();
                          artist->discogsID = o["id"].toInt();
                          artist->title = o["title"].toString();
                          artist->absoluteUri = o["resource_url"].toString();
                          artist->uri = o["uri"].toString();
                          if ( !o["thumb"].isNull() ) {
                            artist->thumb = o["thumb"].toString();
                          }
                          return artist;
                        } );
                        cb( parsed );
                      } )
      ->run();
}

void Provider::getArtist( int id, Callback::Artist cb ) {
  if ( cache_->artists_.find( id ) != cache_->artists_.end() ) {
    cb( cache_->artists_[id] );
    return;
  }
  backend_
      ->getArtist( id,
                   [this, id, cb]( const QJsonDocument &doc ) {
                     auto o = doc.object();
                     auto artist = new Model::Artist();
                     if ( o["name"].isString() )
                       artist->title = o["name"].toString();
                     if ( o["profile"].isString() )
                       artist->bio = o["profile"].toString();
                     cache_->artists_[id] = artist;
                     cb( artist );
                   } )
      ->run();
}

void Provider::getArtistReleases( Model::Artist *artist, Callback::ReleasableList cb ) {
  if ( cache_->artistReleases_.find( artist->discogsID ) != cache_->artistReleases_.end() ) {
    cb( cache_->artistReleases_[artist->discogsID] );
    return;
  }
  backend_
      ->getArtistReleases( artist->discogsID,
                           [this, artist, cb]( const QJsonDocument &doc ) {
                             std::vector<Model::Releasable *> parsed;
                             auto releases = doc.object()["releases"].toArray();
                             std::transform( releases.begin(), releases.end(), std::back_inserter( parsed ), [this, artist]( const QJsonValue &e ) {
                               auto o = e.toObject();
                               Model::Releasable *result;
                               if ( o["type"].toString() == "master" ) {
                                 auto master = new Model::Master;
                                 master->artist = artist;
                                 master->discogsID = o["id"].toInt();
                                 master->title = o["title"].toString();
                                 master->format = o["format"].toString();
                                 master->uri = o["resource_url"].toString();
                                 master->year = o["year"].toInt();
                                 master->mainReleaseID = o["main_release"].toInt();
                                 if ( cache_->masters_.find( master->discogsID ) == cache_->masters_.end() ) {
                                   cache_->masters_[master->discogsID] = master;
                                 }
                                 result = master;
                               } else {
                                 auto release = new Model::Release;
                                 release->artist = artist;
                                 release->discogsID = o["id"].toInt();
                                 release->title = o["title"].toString();
                                 release->format = o["format"].toString();
                                 release->uri = o["resource_url"].toString();
                                 release->year = o["year"].toInt();
                                 if ( cache_->releases_.find( release->discogsID ) == cache_->releases_.end() ) {
                                   cache_->releases_[release->discogsID] = release;
                                 }
                                 result = release;
                               }
                               return result;
                             } );
                             cache_->artistReleases_[artist->discogsID] = parsed;
                             cb( parsed );
                           } )
      ->run();
}

Model::Track parseTrack( const QJsonObject &t ) {
  Model::Track track;
  track.position = t["position"].toString();
  track.title = t["title"].toString();
  track.duration = parseDuration( t["duration"].toString() );
  if ( t.contains( "artists" ) ) {
    auto artists = t["artists"].toArray();
    int length = std::accumulate( artists.begin(), artists.end(), 0, []( int sum, const QJsonValue &a ) {
      return sum + a.toObject()["name"].toString().size() + a.toObject()["join"].toString().size();
    } );
    track.artist.reserve( length );
    std::for_each( artists.begin(), artists.end() - 1, [&track]( const QJsonValue &a ) {
      track.artist += a.toObject()["name"].toString();
      track.artist += a.toObject()["join"].toString();
    } );
    track.artist += artists.last().toObject()["name"].toString();
  }
  return track;
}

QString parseReleaseFormats( QJsonValue formats ) {
  if ( formats.isNull() ) {
    return "";
  }
  if ( !formats.isArray() ) {
    return "";
  }
  QStringList list;
  for ( auto format : formats.toArray() ) {
    auto formatObj = format.toObject();
    QString f = formatObj["name"].toString();
    if ( formatObj["descriptions"].isArray() ) {
      for ( auto d : formatObj["descriptions"].toArray() ) {
        f += ", " + d.toString();
      }
    }
    list << f;
  }
  return list.join( "; " );
}

void Provider::getRelease( int id, Callback::Release cb ) {
  if ( cache_->releaseDetailsLoaded_[id] ) {
    cb( cache_->releases_[id] );
    return;
  }
  backend_
      ->getRelease( id,
                    [this, id, cb]( const QJsonDocument &doc ) {
                      auto o = doc.object();
                      qDebug() << o;
                      Model::Release *release;
                      if ( cache_->releases_.find( id ) == cache_->releases_.end() ) {
                        release = new Model::Release();
                        release->discogsID = o["id"].toInt();
                        release->format = parseReleaseFormats( o["formats"] );
                        release->title = o["title"].toString();
                        release->uri = o["uri"].toString();
                        release->year = o["year"].toInt();
                        auto artist = o["artists"].toArray()[0].toObject();
                        auto artistID = artist["id"].toInt();
                        if ( cache_->artists_.find( artistID ) != cache_->artists_.end() ) {
                          release->artist = cache_->artists_[artistID];
                        } else {
                          auto a = new Model::Artist();
                          a->discogsID = artistID;
                          a->title = artist["name"].toString();
                          a->uri = artist["resource_url"].toString();
                          release->artist = a;
                        }
                      } else {
                        release = cache_->releases_[id];
                      }
                      if ( o.contains( "tracklist" ) ) {
                        auto trackArray = o["tracklist"].toArray();
                        std::transform( trackArray.begin(), trackArray.end(), std::back_inserter( release->tracks ),
                                        []( const QJsonValue &e ) { return parseTrack( e.toObject() ); } );
                        std::for_each( release->tracks.begin(), release->tracks.end(), [release]( Model::Track &track ) {
                          track.releaseTitle = release->title;
                          if ( track.artist.isEmpty() ) {
                            track.artist = release->artist->title;
                          }
                        } );
                      }
                      cache_->releases_[id] = release;
                      cache_->releaseDetailsLoaded_[id] = true;
                      cb( release );
                    } )
      ->run();
}

void Provider::getMaster( Model::Artist *artist, int id, Callback::Master cb ) {
  if ( cache_->masterDetailsLoaded_[id] ) {
    cb( cache_->masters_[id] );
    return;
  }
  backend_
      ->getMaster( id,
                   [this, artist, id, cb]( const QJsonDocument &doc ) {
                     auto o = doc.object();
                     Model::Master *master;
                     if ( cache_->masters_.find( id ) == cache_->masters_.end() ) {
                       master = new Model::Master();
                     } else {
                       master = cache_->masters_[id];
                     }
                     if ( o.contains( "tracklist" ) ) {
                       auto trackArray = o["tracklist"].toArray();
                       std::transform( trackArray.begin(), trackArray.end(), std::back_inserter( master->tracks ),
                                       []( const QJsonValue &e ) { return parseTrack( e.toObject() ); } );
                       std::for_each( master->tracks.begin(), master->tracks.end(), [artist]( Model::Track &track ) {
                         if ( track.artist.isEmpty() )
                           track.artist = artist->title;
                       } );
                     }
                     cache_->masters_[id] = master;
                     cache_->masterDetailsLoaded_[id] = true;
                     cb( master );
                   } )
      ->run();
}

void Provider::getMasterVersions( int id, Callback::MasterVersionList cb ) {
  if ( cache_->masterVersions_.find( id ) != cache_->masterVersions_.end() ) {
    cb( cache_->masterVersions_[id] );
    return;
  }
  backend_
      ->getMasterVersions( id,
                           [this, id, cb]( const QJsonDocument &doc ) {
                             auto versions = doc.object()["versions"].toArray();
                             std::map<int, Model::MasterVersion *> result;
                             std::transform( versions.begin(), versions.end(), std::inserter( result, result.end() ), []( const QJsonValue &v ) {
                               auto version = v.toObject();
                               auto result = new Model::MasterVersion();
                               result->discogsID = version["id"].toInt();
                               result->title = version["title"].toString();
                               result->country = version["country"].toString();
                               result->label = version["label"].toString();
                               result->released = version["released"].toString();
                               result->format = version["format"].toString();
                               result->catalogNo = version["catno"].toString();
                               return std::make_pair( result->discogsID, result );
                             } );
                             cache_->masterVersions_[id] = result;
                             cb( result );
                           } )
      ->run();
}

void Provider::loadCollection( Callback::ReleasableList cb ) {
  backend_
      ->loadCollection( [cb]( const QJsonDocument &doc ) {
        std::vector<Model::Releasable *> parsed;
        auto releases = doc.object()["releases"].toArray();
        std::transform( releases.begin(), releases.end(), std::back_inserter( parsed ), []( const QJsonValue &e ) {
          auto o = e.toObject();
          auto basic = o["basic_information"].toObject();
          auto release = new Model::Release();
          release->discogsID = basic["id"].toInt();
          release->title = basic["title"].toString();
          release->year = basic["year"].toInt();
          release->artist = new Model::Artist();
          release->artist->title = basic["artists"].toArray()[0].toObject()["name"].toString();
          return release;
        } );
        cb( parsed );
      } )
      ->run();
}

void Provider::saveReleaseToCollection( int id ) { backend_->saveReleaseToCollection( id )->run(); }

} // namespace Discogs
