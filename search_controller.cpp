#include "search_controller.h"
#include "artist_controller.h"
#include "discogs_provider.h"
#include "icontrollerhost.h"
#include "searchview.h"

#include <QJsonArray>
#include <QJsonObject>

#include <QDebug>

namespace Search {

Controller::Controller( QString term ) : BaseController(), term_( term ), view_( new View() ) { view_->setTerm( term_ ); }

Controller::~Controller() { view_->deleteLater(); }

QWidget *Controller::view() { return view_; }

void Controller::execute( IControllerHost *host ) {
  view_->artistClicked.connect( [host]( Model::Artist *artist ) { host->activateController( new Artist::Controller( artist ) ); } );
  host->discogs()->artistSearch( term_, [this]( const std::vector<Model::Artist *> &artists ) {
    std::for_each( artists.begin(), artists.end(), [this]( Model::Artist *artist ) { view_->addArtist( artist ); } );
  } );
}
}
