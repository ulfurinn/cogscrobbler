#ifndef TEXTEDITLOGGER_H
#define TEXTEDITLOGGER_H

#include <QtGlobal>

class QTextEdit;

class TextEditLogger {
public:
  TextEditLogger( QTextEdit * );
  static void handler( QtMsgType, const QMessageLogContext &, const QString & );

private:
  QTextEdit *edit_;
  static TextEditLogger *instance_;
  void log( QtMsgType, const QMessageLogContext &, const QString & );
};

#endif // TEXTEDITLOGGER_H
