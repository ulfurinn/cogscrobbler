#include "sidebaritem.h"

SidebarItem::SidebarItem( QString title ) : QObject( nullptr ), parent_( nullptr ), title_( title ) {}

const QString &SidebarItem::title() { return title_; }

SidebarItem *SidebarItem::child( int n ) { return children_[n]; }

SidebarItem *SidebarItem::parent() { return parent_; }

void SidebarItem::setParent( SidebarItem *parent ) { parent_ = parent; }

int SidebarItem::row() {
  if ( parent_ )
    return parent_->indexOf( this );
  return -1;
}

unsigned int SidebarItem::size() { return children_.size(); }

void SidebarItem::append( SidebarItem *child ) {
  child->setParent( this );
  children_.push_back( child );
}

int SidebarItem::indexOf( SidebarItem *child ) {
  for ( unsigned int i = 0; i < size(); i++ ) {
    if ( children_[i] == child )
      return i;
  }
  return -1;
}

void SidebarItem::activate() {
  if ( action_ )
    action_();
}
