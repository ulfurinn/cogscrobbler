#ifndef MODELS_H
#define MODELS_H

#include "multifunction.h"
#include <QString>
#include <map>
#include <vector>

namespace Model {

struct Artist {
  int discogsID;
  QString title;
  QString bio;
  QString absoluteUri;
  QString uri;
  QString thumb;
};

struct Track {
  QString position;
  int duration;
  QString title;
  QString artist;
  QString releaseTitle;
};

struct Release;
struct Master;

struct Releasable {
  typedef multifunction<void( Model::Release * ), void( Model::Master * )> Visitor;
  int discogsID;
  QString title;
  QString format;
  int year;
  QString uri;
  Artist *artist;
  std::vector<Track> tracks;
  virtual void apply( Visitor ) = 0;
};

struct MasterVersion {
  int discogsID;
  QString format;
  QString title;
  QString country;
  QString label;
  QString catalogNo;
  QString released;
};

struct Master : public Releasable {
  int mainReleaseID;
  std::map<int, MasterVersion *> versions;
  virtual void apply( Visitor ) override;
};

struct Release : public Releasable {
  virtual void apply( Visitor ) override;
};

struct DiscogsCollection {
  int discogsID;
  QString name;
};
}
#endif // MODELS_H
