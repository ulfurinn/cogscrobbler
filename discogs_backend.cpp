#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QUrlQuery>

#include "discogs_backend.h"

#include "common.h"
#include "http_request.h"
#include "secret.h"
#include "utils.h"

namespace Discogs {

class DiscogsBaseRequest : public HTTPRequest {
protected:
  Backend *discogs_;

public:
  DiscogsBaseRequest( Backend *d ) : HTTPRequest(), discogs_( d ) {}

  virtual void handleError( QNetworkReply::NetworkError ) { qCritical() << "error" << reply_->errorString(); }

protected:
  void commonAuthenticatedRequest( QNetworkRequest &request ) {
    request.setHeader( QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded" );
    request.setHeader( QNetworkRequest::UserAgentHeader, USER_AGENT );
    request.setRawHeader( QByteArray( "Authorization" ), buildOAuth() );
  }
  virtual QByteArray buildOAuth() {
    QByteArray a;
    a.append( "OAuth " );
    a.append( "oauth_consumer_key=\"" ).append( DISCOGS_CONSUMER_KEY ).append( "\"," );
    a.append( "oauth_nonce=\"" ).append( randomString( 48 ) ).append( "\"," );
    a.append( "oauth_signature=\"" ).append( DISCOGS_CONSUMER_SECRET ).append( "&" ).append( discogs_->secret() ).append( "\"," );
    a.append( "oauth_signature_method=\"PLAINTEXT\"," );
    a.append( "oauth_token=\"" ).append( discogs_->token() ).append( "\"," );
    a.append( "oauth_timestamp=\"" ).append( QString::number( QDateTime::currentDateTimeUtc().toTime_t() ) ).append( "\"" );
    return a;
  }
};

class DiscogsBeginAuthRequest : public DiscogsBaseRequest {
public:
  using DiscogsBaseRequest::DiscogsBaseRequest;

  QByteArray buildOAuth() {
    QByteArray a;
    a.append( "OAuth " );
    a.append( "oauth_consumer_key=\"" ).append( DISCOGS_CONSUMER_KEY ).append( "\"," );
    a.append( "oauth_nonce=\"" ).append( randomString( 48 ) ).append( "\"," );
    a.append( "oauth_signature=\"" ).append( DISCOGS_CONSUMER_SECRET ).append( "&\"," );
    a.append( "oauth_signature_method=\"PLAINTEXT\"," );
    a.append( "oauth_timestamp=\"" ).append( QString::number( QDateTime::currentDateTimeUtc().toTime_t() ) ).append( "\"" );
    return a;
  }

  virtual QNetworkReply *startRequest() {
    QNetworkRequest request;
    commonAuthenticatedRequest( request );
    request.setUrl( QUrl( "https://api.discogs.com/oauth/request_token" ) );
    QSslConfiguration sslConf;
    sslConf.setProtocol( QSsl::SecureProtocols );
    request.setSslConfiguration( sslConf );
    return manager->get( request );
  }
  virtual void handleResponse() {
    if ( reply_->error() != QNetworkReply::NoError )
      return;

    QString body( reply_->readAll() );
    QUrlQuery q( body );
    if ( q.hasQueryItem( "oauth_token" ) && q.hasQueryItem( "oauth_token_secret" ) ) {
      discogs_->authFirstStepPassed( q.queryItemValue( "oauth_token" ), q.queryItemValue( "oauth_token_secret" ) );
    }
  }
};

class DiscogsCompleteAuthRequest : public DiscogsBaseRequest {
public:
  using DiscogsBaseRequest::DiscogsBaseRequest;
  QByteArray buildOAuth() {
    QByteArray a;
    a.append( "OAuth " );
    a.append( "oauth_consumer_key=\"" ).append( DISCOGS_CONSUMER_KEY ).append( "\"," );
    a.append( "oauth_nonce=\"" ).append( randomString( 48 ) ).append( "\"," );
    a.append( "oauth_signature=\"" ).append( DISCOGS_CONSUMER_SECRET ).append( "&" ).append( discogs_->secret() ).append( "\"," );
    a.append( "oauth_signature_method=\"PLAINTEXT\"," );
    a.append( "oauth_timestamp=\"" ).append( QString::number( QDateTime::currentDateTimeUtc().toTime_t() ) ).append( "\"," );
    a.append( "oauth_token=\"" ).append( discogs_->token() ).append( "\"," );
    a.append( "oauth_verifier=\"" ).append( discogs_->verification() ).append( "\"" );
    return a;
  }

  virtual QNetworkReply *startRequest() {
    QNetworkRequest request;
    commonAuthenticatedRequest( request );
    request.setUrl( QUrl( "https://api.discogs.com/oauth/access_token" ) );
    QSslConfiguration sslConf;
    sslConf.setProtocol( QSsl::SecureProtocols );
    request.setSslConfiguration( sslConf );

    return manager->post( request, QByteArray() );
  }
  virtual void handleResponse() {
    if ( reply_->error() != QNetworkReply::NoError )
      return;

    QString body( reply_->readAll() );
    QUrlQuery q( body );
    if ( q.hasQueryItem( "oauth_token" ) && q.hasQueryItem( "oauth_token_secret" ) ) {
      discogs_->setCredentials( q.queryItemValue( "oauth_token" ), q.queryItemValue( "oauth_token_secret" ) );
      discogs_->verify();
    }
  }
};

class DiscogsIdentityRequest : public DiscogsBaseRequest {
public:
  using DiscogsBaseRequest::DiscogsBaseRequest;
  virtual QNetworkReply *startRequest() {
    QNetworkRequest request;
    commonAuthenticatedRequest( request );
    request.setUrl( QUrl( "https://api.discogs.com/oauth/identity" ) );
    QSslConfiguration sslConf;
    sslConf.setProtocol( QSsl::SecureProtocols );
    request.setSslConfiguration( sslConf );

    return manager->get( request );
  }

  // HTTPRequest interface
public:
  virtual void handleResponse() {
    if ( reply_->error() != QNetworkReply::NoError ) {
      qCritical() << "Backend identity error";
      discogs_->unauthorized();
      return;
    }

    auto body = reply_->readAll();
    qDebug() << QString( body );
    QJsonParseError err;
    auto response = QJsonDocument::fromJson( body, &err );
    if ( err.error != QJsonParseError::NoError ) {
      qCritical() << err.errorString();
      return;
    }
    auto obj = response.object();
    auto username = obj["username"].toString();
    discogs_->verified( username );
    discogs_->fetchCollections();
  }
};

class BaseAction : public DiscogsBaseRequest, public Discogs::Action {
public:
  using DiscogsBaseRequest::DiscogsBaseRequest;
  void run() { execute(); }
};

class ArtistSearchAction : public BaseAction {
private:
  QString term_;
  Callback::Generic cb_;

public:
  ArtistSearchAction( Backend *d, QString term, Callback::Generic callback ) : BaseAction( d ), term_( term ), cb_( callback ) {}

  // HTTPRequest interface
public:
  virtual QNetworkReply *startRequest() {
    QNetworkRequest request;
    commonAuthenticatedRequest( request );
    QUrl url( "https://api.discogs.com/database/search" );
    QUrlQuery q;
    q.addQueryItem( "q", term_ );
    q.addQueryItem( "type", "artist" );
    url.setQuery( q );
    qDebug() << url.toString();
    request.setUrl( url );
    QSslConfiguration sslConf;
    sslConf.setProtocol( QSsl::SecureProtocols );
    request.setSslConfiguration( sslConf );

    return manager->get( request );
  }
  virtual void handleResponse() {
    auto body = reply_->readAll();
    QJsonParseError err;
    auto resp = QJsonDocument::fromJson( body, &err );
    if ( err.error != QJsonParseError::NoError ) {
      qCritical() << err.errorString();
      return;
    }
    cb_( resp );
  }
};

class ArtistFetchAction : public BaseAction {
private:
  int id_;
  Callback::Generic cb_;

public:
  ArtistFetchAction( Backend *d, int id, Callback::Generic callback ) : BaseAction( d ), id_( id ), cb_( callback ) {}

  // HTTPRequest interface
public:
  virtual QNetworkReply *startRequest() {
    QNetworkRequest request;
    commonAuthenticatedRequest( request );
    QUrl url( QStringLiteral( "https://api.discogs.com/artists/%1" ).arg( id_ ) );
    request.setUrl( url );
    QSslConfiguration sslConf;
    sslConf.setProtocol( QSsl::SecureProtocols );
    request.setSslConfiguration( sslConf );

    return manager->get( request );
  }
  virtual void handleResponse() {
    auto body = reply_->readAll();
    QJsonParseError err;
    auto resp = QJsonDocument::fromJson( body, &err );
    if ( err.error != QJsonParseError::NoError ) {
      qCritical() << err.errorString();
      return;
    }
    cb_( resp );
  }
};

class ArtistReleaseFetchAction : public BaseAction {
private:
  int id_;
  Callback::Generic cb_;

public:
  ArtistReleaseFetchAction( Backend *d, int id, Callback::Generic callback ) : BaseAction( d ), id_( id ), cb_( callback ) {}

  // HTTPRequest interface
public:
  virtual QNetworkReply *startRequest() {
    QNetworkRequest request;
    commonAuthenticatedRequest( request );
    QUrl url( QStringLiteral( "https://api.discogs.com/artists/%1/releases" ).arg( id_ ) );
    request.setUrl( url );
    QSslConfiguration sslConf;
    sslConf.setProtocol( QSsl::SecureProtocols );
    request.setSslConfiguration( sslConf );

    return manager->get( request );
  }
  virtual void handleResponse() {
    auto body = reply_->readAll();
    QJsonParseError err;
    auto resp = QJsonDocument::fromJson( body, &err );
    if ( err.error != QJsonParseError::NoError ) {
      qCritical() << err.errorString();
      return;
    }
    cb_( resp );
  }
};

class ReleaseFetchAction : public BaseAction {
private:
  int id_;
  Callback::Generic cb_;

public:
  ReleaseFetchAction( Backend *d, int id, Callback::Generic cb ) : BaseAction( d ), id_( id ), cb_( cb ) {}

  // HTTPRequest interface
public:
  virtual QNetworkReply *startRequest() {
    QNetworkRequest request;
    commonAuthenticatedRequest( request );
    QUrl url( QStringLiteral( "https://api.discogs.com/releases/%1" ).arg( id_ ) );
    qDebug() << url.toString();
    request.setUrl( url );
    QSslConfiguration sslConf;
    sslConf.setProtocol( QSsl::SecureProtocols );
    request.setSslConfiguration( sslConf );

    return manager->get( request );
  }
  virtual void handleResponse() {
    auto body = reply_->readAll();
    QJsonParseError err;
    auto resp = QJsonDocument::fromJson( body, &err );
    if ( err.error != QJsonParseError::NoError ) {
      qCritical() << err.errorString();
      return;
    }
    cb_( resp );
  }
};

class MasterFetchAction : public BaseAction {
private:
  int id_;
  Callback::Generic cb_;

public:
  MasterFetchAction( Backend *d, int id, Callback::Generic cb ) : BaseAction( d ), id_( id ), cb_( cb ) {}

  // HTTPRequest interface
public:
  virtual QNetworkReply *startRequest() {
    QNetworkRequest request;
    commonAuthenticatedRequest( request );
    QUrl url( QStringLiteral( "https://api.discogs.com/masters/%1" ).arg( id_ ) );
    qDebug() << url.toString();
    request.setUrl( url );
    QSslConfiguration sslConf;
    sslConf.setProtocol( QSsl::SecureProtocols );
    request.setSslConfiguration( sslConf );

    return manager->get( request );
  }
  virtual void handleResponse() {
    auto body = reply_->readAll();
    QJsonParseError err;
    auto resp = QJsonDocument::fromJson( body, &err );
    if ( err.error != QJsonParseError::NoError ) {
      qCritical() << err.errorString();
      return;
    }
    cb_( resp );
  }
};

class MasterVersionFetchAction : public BaseAction {
private:
  int id_;
  Callback::Generic cb_;

public:
  MasterVersionFetchAction( Backend *d, int id, Callback::Generic cb ) : BaseAction( d ), id_( id ), cb_( cb ) {}

  // HTTPRequest interface
public:
  virtual QNetworkReply *startRequest() {
    QNetworkRequest request;
    commonAuthenticatedRequest( request );
    QUrl url( QStringLiteral( "https://api.discogs.com/masters/%1/versions" ).arg( id_ ) );
    qDebug() << url.toString();
    request.setUrl( url );
    QSslConfiguration sslConf;
    sslConf.setProtocol( QSsl::SecureProtocols );
    request.setSslConfiguration( sslConf );

    return manager->get( request );
  }
  virtual void handleResponse() {
    auto body = reply_->readAll();
    QJsonParseError err;
    auto resp = QJsonDocument::fromJson( body, &err );
    if ( err.error != QJsonParseError::NoError ) {
      qCritical() << err.errorString();
      return;
    }
    cb_( resp );
  }
};

class FetchCollectionsAction : public BaseAction {
public:
  FetchCollectionsAction( Backend *d ) : BaseAction( d ) {}

  // HTTPRequest interface
public:
  virtual QNetworkReply *startRequest() {
    QNetworkRequest request;
    commonAuthenticatedRequest( request );
    QUrl url( QStringLiteral( "https://api.discogs.com/users/%1/collection/folders" ).arg( discogs_->username() ) );
    request.setUrl( url );
    QSslConfiguration sslConf;
    sslConf.setProtocol( QSsl::SecureProtocols );
    request.setSslConfiguration( sslConf );

    return manager->get( request );
  }
  virtual void handleResponse() {
    auto body = reply_->readAll();
    QJsonParseError err;
    auto resp = QJsonDocument::fromJson( body, &err );
    if ( err.error != QJsonParseError::NoError ) {
      qCritical() << err.errorString();
      return;
    }
    std::vector<Model::DiscogsCollection> folders;
    for ( auto j : resp.object()["folders"].toArray() ) {
      auto jFolder = j.toObject();
      Model::DiscogsCollection folder;
      folder.discogsID = jFolder["id"].toInt();
      folder.name = jFolder["name"].toString();
      folders.push_back( folder );
    }
    discogs_->collections( folders );
  }
};

class LoadCollectionAction : public BaseAction {
private:
  Callback::Generic cb_;

public:
  LoadCollectionAction( Backend *d, Callback::Generic cb ) : BaseAction( d ), cb_( cb ) {}

  // HTTPRequest interface
public:
  virtual QNetworkReply *startRequest() {
    QNetworkRequest request;
    commonAuthenticatedRequest( request );
    QUrl url(
        QStringLiteral( "https://api.discogs.com/users/%1/collection/folders/%2/releases" ).arg( discogs_->username() ).arg( discogs_->readCollection() ) );
    request.setUrl( url );
    QSslConfiguration sslConf;
    sslConf.setProtocol( QSsl::SecureProtocols );
    request.setSslConfiguration( sslConf );

    return manager->get( request );
  }
  virtual void handleResponse() {
    auto body = reply_->readAll();
    QJsonParseError err;
    auto resp = QJsonDocument::fromJson( body, &err );
    if ( err.error != QJsonParseError::NoError ) {
      qCritical() << err.errorString();
      return;
    }
    cb_( resp );
  }
};

class SaveReleaseToCollectionAction : public BaseAction {
private:
  int id_;

public:
  SaveReleaseToCollectionAction( Backend *d, int id ) : BaseAction( d ), id_( id ) {}

  // HTTPRequest interface
public:
  virtual QNetworkReply *startRequest() override {
    QNetworkRequest request;
    commonAuthenticatedRequest( request );
    QUrl url( QStringLiteral( "https://api.discogs.com/users/%1/collection/folders/%2/releases/%3" )
                  .arg( discogs_->username() )
                  .arg( discogs_->writeCollection() )
                  .arg( id_ ) );
    request.setUrl( url );
    QSslConfiguration sslConf;
    sslConf.setProtocol( QSsl::SecureProtocols );
    request.setSslConfiguration( sslConf );

    return manager->post( request, QByteArray() );
  }
  virtual void handleResponse() override {}
};

Backend::Backend() {}

void Backend::verify() {
  if ( token_ == "" || secret_ == "" ) {
    emit unauthorized();
    return;
  }
  auto req = new DiscogsIdentityRequest( this );
  req->execute();
}

void Backend::fetchCollections() { ( new FetchCollectionsAction( this ) )->execute(); }

QString Backend::token() { return token_; }

QString Backend::secret() { return secret_; }

QString Backend::verification() { return verification_; }

const QString &Backend::username() { return username_; }

int Backend::readCollection() { return readCollection_; }

int Backend::writeCollection() { return writeCollection_; }

void Backend::authFirstStepPassed( const QString &token, const QString &secret ) {
  token_ = token;
  secret_ = secret;
  emit authSecondStep( token_ );
}

void Backend::verified( const QString &username ) {
  username_ = username;
  authorized( token_, secret_, username_ );
}

Action *Backend::artistSearch( const QString &term, Callback::Generic callback ) { return new ArtistSearchAction( this, term, callback ); }

Action *Backend::getArtist( int id, Callback::Generic callback ) { return new ArtistFetchAction( this, id, callback ); }

Action *Backend::getArtistReleases( int id, Callback::Generic callback ) { return new ArtistReleaseFetchAction( this, id, callback ); }

Action *Backend::getRelease( int id, Callback::Generic callback ) { return new ReleaseFetchAction( this, id, callback ); }

Action *Backend::getMaster( int id, Callback::Generic callback ) { return new MasterFetchAction( this, id, callback ); }

Action *Backend::getMasterVersions( int id, Callback::Generic callback ) { return new MasterVersionFetchAction( this, id, callback ); }

Action *Backend::loadCollection( Callback::Generic callback ) { return new LoadCollectionAction( this, callback ); }

Action *Backend::saveReleaseToCollection( int id ) { return new SaveReleaseToCollectionAction( this, id ); }

void Backend::setCredentials( const QString &token, const QString &secret ) {
  token_ = token;
  secret_ = secret;
}

void Backend::setReadCollection( int id ) { readCollection_ = id; }

void Backend::setWriteCollection( int id ) { writeCollection_ = id; }

void Backend::initiateAuth() {
  auto req = new DiscogsBeginAuthRequest( this );
  req->execute();
}

void Backend::completeAuth( const QString &key ) {
  verification_ = key;
  auto req = new DiscogsCompleteAuthRequest( this );
  req->execute();
}
}
