#include "artist_view.h"
#include "release_button.h"
#include "ui_view_artist.h"

namespace Artist {

View::View( QWidget *parent ) : QFrame( parent ), ui( new Ui_View ), empty_( nullptr ) { ui->setupUi( this ); }

View::~View() { delete ui; }

void View::setName( const QString &name ) { ui->labTitle->setText( name ); }

void View::setBio( const QString &bio ) { ui->labBio->setText( bio ); }

void View::addRelease( Model::Master *master ) {
  createDummy();
  auto btn = new ReleaseButton( ui->cntReleases );
  connect( btn, &ReleaseButton::clicked, [this, master]() { masterClicked( master ); } );
  btn->setRelease( master );
  ui->verticalLayout_5->addWidget( btn );
  ui->verticalLayout_5->addWidget( empty_ );
}

void View::addRelease( Model::Release *release ) {
  createDummy();
  auto btn = new ReleaseButton( ui->cntReleases );
  connect( btn, &ReleaseButton::clicked, [this, release]() { releaseClicked( release ); } );
  btn->setRelease( release );
  ui->verticalLayout_5->addWidget( btn );
  ui->verticalLayout_5->addWidget( empty_ );
}

void View::createDummy() {
  if ( empty_ == nullptr ) {
    empty_ = new QWidget( ui->scrollAreaWidgetContents_2 );
    empty_->setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Expanding );
  } else
    ui->verticalLayout_5->removeWidget( empty_ );
}

} // namespace View
