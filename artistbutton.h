#ifndef ARTISTBUTTON_H
#define ARTISTBUTTON_H

#include <QFrame>
#include <QMouseEvent>

namespace Ui {
class ArtistButton;
}

class ArtistButton : public QFrame {
  Q_OBJECT

public:
  explicit ArtistButton( QWidget *parent = 0 );
  ~ArtistButton();

  void setText( const QString &title );
  void setImageUrl( const QString &url );

signals:
  void clicked();

private:
  Ui::ArtistButton *ui;

  // QWidget interface
protected:
  virtual void mousePressEvent( QMouseEvent * );
};

#endif // ARTISTBUTTON_H
