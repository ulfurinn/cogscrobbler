#ifndef DISCOGS_CALLBACK_H
#define DISCOGS_CALLBACK_H

#include "models.h"
#include <QJsonDocument>
#include <vector>

namespace Discogs {
namespace Callback {
typedef std::function<void( const QJsonDocument & )> Generic;
typedef std::function<void( const std::vector<Model::Artist *> & )> ArtistList;
typedef std::function<void( const std::vector<Model::Releasable *> & )> ReleasableList;
typedef std::function<void( const std::map<int, Model::MasterVersion *> & )> MasterVersionList;

typedef std::function<void( Model::Artist * )> Artist;
typedef std::function<void( Model::Release * )> Release;
typedef std::function<void( Model::Master * )> Master;
}
}

#endif // DISCOGS_CALLBACK_H
