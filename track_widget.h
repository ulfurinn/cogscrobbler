#ifndef TRACK_WIDGET_H
#define TRACK_WIDGET_H

#include <QWidget>

namespace Ui {
class TrackWidget;
}

class TrackWidget : public QWidget {
  Q_OBJECT

public:
  explicit TrackWidget( QWidget *parent = 0 );
  ~TrackWidget();

  void setPosition( const QString &position );
  void setTitle( const QString &title );
  void setArtist( const QString &artist );
  void setDuration( int duration );
  void setDuration( const QString &duration );

signals:
  void selected( bool );
  void durationEdited( int seconds );

private:
  Ui::TrackWidget *ui;
  int duration_;

  void showDurationEditor();
  void hideDurationEditor();
};

#endif // TRACK_WIDGET_H
