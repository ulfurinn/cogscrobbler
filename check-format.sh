#!/bin/sh

clang-format -style file $1 | diff $1 - > /dev/null
ok=$?
if [ $ok != 0 ]
then
  echo $1 is not formatted.
fi
exit $ok
