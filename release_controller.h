#ifndef RELEASE_CONTROLLER_H
#define RELEASE_CONTROLLER_H

#include "base_controller.h"
#include "models.h"
#include <QObject>
#include <functional>

namespace Discogs {
class Provider;
}

namespace Release {
class View;
class Controller : public BaseController {
public:
  explicit Controller( Model::Release *release );
  ~Controller();

  typedef std::function<void( std::vector<Model::Track>, uint )> scrobbleActionF;
  typedef std::function<void( Model::Release * )> saveActionF;

private:
  void trackSelectionChanged( int, bool );
  void trackDurationChanged( int, int );

  // BaseController interface
public:
  virtual void execute( IControllerHost * ) override;
  virtual QWidget *view() override;

private:
  Release::View *view_;
  Model::Release *release_;
  std::map<int, bool> selected_;
};

} // namespace Release

#endif // RELEASE_CONTROLLER_H
