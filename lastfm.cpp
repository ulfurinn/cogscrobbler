#include "lastfm.h"
#include "common.h"
#include "http_request.h"
#include "secret.h"
#include "utils.h"

#include <QJsonObject>
#include <QJsonParseError>
#include <QUrlQuery>

class flatten {
public:
  flatten() {}
  void operator()( const QPair<QString, QString> &pair ) { acc_ += pair.first + pair.second; }
  QString result() { return acc_; }

private:
  QString acc_;
};

class LastFMRequest : public HTTPRequest {

public:
  LastFMRequest() {}

  virtual void handleError( QNetworkReply::NetworkError ) { qCritical() << "error" << reply_->errorString(); }

protected:
  QNetworkRequest baseRequest() {
    QNetworkRequest request;
    request.setHeader( QNetworkRequest::UserAgentHeader, USER_AGENT );
    request.setRawHeader( QByteArray( "Host" ), "ws.audioscrobbler.com" );
    return request;
  }

  QNetworkRequest commonRequest() {
    auto request = baseRequest();
    QUrl url( "http://ws.audioscrobbler.com/2.0/" );
    request.setUrl( url );
    return request;
  }
  QNetworkRequest commonRequest( QUrlQuery &q ) {
    auto request = baseRequest();
    QUrl url( "http://ws.audioscrobbler.com/2.0/" );
    url.setQuery( q );
    request.setUrl( url );
    return request;
  }
  QString sign( QUrlQuery &q ) {
    std::vector<QPair<QString, QString>> params;
    for ( auto pair : q.queryItems() ) {
      params.push_back( pair );
    }
    std::sort( params.begin(), params.end(), []( QPair<QString, QString> a, QPair<QString, QString> b ) { return a.first < b.first; } );
    QString flat = std::for_each( params.begin(), params.end(), flatten() ).result();
    flat += LASTFM_SECRET;
    return md5( flat );
  }

  void commonQuery( QUrlQuery &q ) {
    q.addQueryItem( "api_key", LASTFM_KEY );
    q.addQueryItem( "api_sig", sign( q ) );
    q.addQueryItem( "format", "json" );
  }
};

class LastFMBeginAuthRequest : public LastFMRequest {
private:
  LastFM *lfm_;

public:
  LastFMBeginAuthRequest( LastFM *lfm ) : lfm_( lfm ) {}

  // HTTPRequest interface
public:
  virtual QNetworkReply *startRequest() {
    QUrlQuery q;
    q.addQueryItem( "method", "auth.getToken" );
    commonQuery( q );
    return manager->get( commonRequest( q ) );
  }
  virtual void handleResponse() {
    if ( reply_->error() != QNetworkReply::NoError )
      return;

    auto body = reply_->readAll();
    qDebug() << QString( body );
    QJsonParseError err;
    auto response = QJsonDocument::fromJson( body, &err );
    if ( err.error != QJsonParseError::NoError ) {
      qCritical() << "last.fm" << err.errorString();
      return;
    }
    auto obj = response.object();
    auto token = obj["token"].toString();
    lfm_->authFirstStepPassed( token );
  }
};

class LastFMCompleteAuthRequest : public LastFMRequest {
private:
  LastFM *lfm_;

public:
  LastFMCompleteAuthRequest( LastFM *lfm ) : lfm_( lfm ) {}

  // HTTPRequest interface
public:
  virtual QNetworkReply *startRequest() {
    QUrlQuery q;
    q.addQueryItem( "method", "auth.getSession" );
    q.addQueryItem( "token", lfm_->token() );
    commonQuery( q );
    return manager->get( commonRequest( q ) );
  }
  virtual void handleResponse() {
    if ( reply_->error() != QNetworkReply::NoError )
      return;

    auto body = reply_->readAll();
    QJsonParseError err;
    auto response = QJsonDocument::fromJson( body, &err );
    if ( err.error != QJsonParseError::NoError ) {
      qCritical() << err.errorString();
      return;
    }
    auto obj = response.object();
    auto session = obj["session"].toObject();
    auto key = session["key"].toString();
    lfm_->setCredentials( key );
    lfm_->verify();
  }
};

class LastFMUserInfoRequest : public LastFMRequest {
private:
  LastFM *lfm_;

public:
  LastFMUserInfoRequest( LastFM *lfm ) : lfm_( lfm ) {}

  // HTTPRequest interface
public:
  virtual QNetworkReply *startRequest() {
    QUrlQuery q;
    q.addQueryItem( "method", "user.getInfo" );
    q.addQueryItem( "sk", lfm_->sessionKey() );
    commonQuery( q );
    qDebug() << q.toString( QUrl::FullyEncoded );
    return manager->get( commonRequest( q ) );
  }
  virtual void handleResponse() {
    if ( reply_->error() != QNetworkReply::NoError ) {
      lfm_->unauthorized();
      return;
    }
    auto body = reply_->readAll();
    qDebug() << QString( body );
    QJsonParseError err;
    auto response = QJsonDocument::fromJson( body, &err );
    if ( err.error != QJsonParseError::NoError ) {
      qCritical() << err.errorString();
      return;
    }
    auto obj = response.object();
    if ( obj.contains( "error" ) ) {
      lfm_->unauthorized();
      return;
    }
    auto user = obj["user"].toObject();
    auto username = user["name"].toString();
    lfm_->verified( username );
  }
};

class LastFMScrobble : public LastFMRequest {
private:
  LastFM *lfm_;
  std::vector<LastFM::ScrobbleItem> batch_;

public:
  LastFMScrobble( LastFM *lfm, std::vector<LastFM::ScrobbleItem> batch ) : lfm_( lfm ), batch_( batch ) {}

  // HTTPRequest interface
public:
  virtual QNetworkReply *startRequest() {
    QUrlQuery q;
    int i = 0;
    for ( auto item : batch_ ) {
      q.addQueryItem( QStringLiteral( "artist[%1]" ).arg( i ), item.artist );
      if ( !item.release.isEmpty() )
        q.addQueryItem( QStringLiteral( "album[%1]" ).arg( i ), item.release );
      q.addQueryItem( QStringLiteral( "track[%1]" ).arg( i ), item.track );
      q.addQueryItem( QStringLiteral( "timestamp[%1]" ).arg( i ), QString::number( item.startTimestamp ) );
      q.addQueryItem( QStringLiteral( "duration[%1]" ).arg( i ), QString::number( item.duration ) );
      i++;
    }
    q.addQueryItem( "method", "track.scrobble" );
    q.addQueryItem( "sk", lfm_->sessionKey() );
    commonQuery( q );
    auto req = commonRequest();
    req.setHeader( QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded" );
    qDebug() << q.toString( QUrl::FullyEncoded ).toUtf8();
    return manager->post( req, q.toString( QUrl::FullyEncoded ).toUtf8() );
  }
  virtual void handleResponse() {
    if ( reply_->error() != QNetworkReply::NoError ) {
      qCritical() << reply_->errorString();
      lfm_->scrobbled( false );
      return;
    }
    auto body = reply_->readAll();
    qDebug() << QString( body );
    QJsonParseError err;
    auto response = QJsonDocument::fromJson( body, &err );
    if ( err.error != QJsonParseError::NoError ) {
      qCritical() << err.errorString();
      lfm_->scrobbled( false );
      return;
    }
    auto obj = response.object();
    qDebug() << obj;
    if ( obj.contains( "scrobbles" ) ) {
      // auto count = obj["scrobbles"].toObject()["@attr"].toObject()["accepted"].toInt();
      // qDebug() << count << "tracks scrobbled";
    }
    lfm_->scrobbled( true );
  }
};

LastFM::LastFM() {
  timer_.connect( &timer_, &QTimer::timeout, [this]() {
    qDebug() << "tick";
    scrobbleBatch();
  } );
  timer_.setSingleShot( true );
}

void LastFM::verify() {
  if ( sessionKey_ == "" ) {
    unauthorized();
    return;
  }
  auto req = new LastFMUserInfoRequest( this );
  req->execute();
}

const QString &LastFM::token() { return token_; }

void LastFM::scrobble( const std::vector<LastFM::ScrobbleItem> &items ) {
  scrobbleQueue_.insert( scrobbleQueue_.end(), items.begin(), items.end() );
  scrobbleBatch();
}

std::vector<LastFM::ScrobbleItem> LastFM::batch( int end ) { return std::vector<LastFM::ScrobbleItem>( scrobbleQueue_.begin(), scrobbleQueue_.begin() + end ); }

void LastFM::scrobbled( bool ok ) {
  pendingScrobble_.clear();
  scrobbleBatch();
}

const QString &LastFM::sessionKey() { return sessionKey_; }

void LastFM::authFirstStepPassed( const QString &token ) {
  token_ = token;
  authSecondStep( token );
}

void LastFM::verified( const QString &username ) { authorized( sessionKey_, username ); }

void LastFM::setCredentials( const QString &key ) { sessionKey_ = key; }

void LastFM::initiateAuth() {
  auto req = new LastFMBeginAuthRequest( this );
  req->execute();
}

void LastFM::completeAuth() {
  auto req = new LastFMCompleteAuthRequest( this );
  req->execute();
}

const int maxScrobble = 50;

void LastFM::scrobbleBatch() {
  if ( !pendingScrobble_.empty() )
    return;
  if ( scrobbleQueue_.empty() )
    return;

  std::vector<ScrobbleItem> ready, pending;
  auto now = QDateTime::currentDateTimeUtc().toTime_t();
  for ( auto item : scrobbleQueue_ ) {
    if ( item.startTimestamp + item.duration <= now && ready.size() < maxScrobble )
      ready.push_back( item );
    else
      pending.push_back( item );
  }

  qDebug() << ready.size() << "items ready for scrobble," << pending.size() << "left for next iteration";
  if ( ready.empty() ) {
    qDebug() << "nothing to scrobble right now";
    timer_.start( 15000 );
  } else {
    pendingScrobble_ = ready;
    scrobbleQueue_ = pending;
    auto req = new LastFMScrobble( this, ready );
    req->execute();
  }
}
