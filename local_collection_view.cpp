#include "local_collection_view.h"
#include "release_button.h"
#include "ui_local_collection_view.h"
#include <QDebug>

namespace LocalCollection {

View::View( QWidget *parent ) : QWidget( parent ), ui( new Ui::View ), empty_( nullptr ) { ui->setupUi( this ); }

View::~View() { delete ui; }

void View::addRelease( Model::Release *release ) {
  createDummy();
  auto btn = new ReleaseButton( ui->cntReleases );
  connect( btn, &ReleaseButton::clicked, [this, release]() {
    qDebug() << "View: release clicked";
    releaseClicked( release );
  } );
  btn->showArtist( true );
  btn->setRelease( release );
  ui->scrollAreaWidgetContents_2->layout()->addWidget( btn );
  ui->scrollAreaWidgetContents_2->layout()->addWidget( empty_ );
}

void View::createDummy() {
  if ( empty_ == nullptr ) {
    empty_ = new QWidget( ui->scrollAreaWidgetContents_2 );
    empty_->setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Expanding );
  } else
    ui->scrollAreaWidgetContents_2->layout()->removeWidget( empty_ );
}

} // namespace LocalCollection
