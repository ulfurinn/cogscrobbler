#ifndef CONTROLLER_ARTIST_H
#define CONTROLLER_ARTIST_H

#include "base_controller.h"
#include "models.h"
#include <functional>

namespace Discogs {
class Provider;
}

namespace Artist {

class View;

class Controller : public BaseController {
public:
  explicit Controller( Model::Artist *artist );
  ~Controller();
  virtual QWidget *view() override;
  virtual void execute( IControllerHost * ) override;

private:
  Model::Artist *artist_;
  View *view_;
};

} // namespace Controller

#endif // CONTROLLER_ARTIST_H
