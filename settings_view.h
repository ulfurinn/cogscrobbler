#ifndef SETTINGS_VIEW_H
#define SETTINGS_VIEW_H

#include "models.h"
#include <QWidget>
#include <boost/signals2.hpp>

namespace Ui {
class settings_view;
}

namespace Settings {
using namespace boost::signals2;
class View : public QWidget {
  Q_OBJECT

public:
  explicit View( QWidget *parent = 0 );
  ~View();

  signal<void()> initiateLastfmLogin;
  signal<void()> completeLastfmLogin;
  signal<void()> resetLastfmLogin;
  signal<void()> initiateDiscogsLogin;
  signal<void( const QString & )> completeDiscogsLogin;
  signal<void()> resetDiscogsLogin;

  void lastfmSessionUser( QString );

  void discogsUnauthorized();
  void discogsAuthSecondStep( const QString &token );
  void discogsAuthorized( const QString &username );
  void discogsCollections( std::vector<Model::DiscogsCollection> );

  void lastfmUnauthorized();
  void lastfmAuthSecondStep( const QString &token );
  void lastfmAuthorized( const QString &username );

  void setReadCollection( int );
  void setWriteCollection( int );

private:
  Ui::settings_view *ui;
  int readCollection_, writeCollection_;
  std::vector<int> readCollections_, writeCollections_;
};
}
#endif // SETTINGS_VIEW_H
