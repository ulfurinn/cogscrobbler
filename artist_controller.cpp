#include "artist_controller.h"
#include "artist_view.h"
#include "discogs_provider.h"
#include "icontrollerhost.h"
#include "master_controller.h"
#include "overload.h"
#include "release_controller.h"
#include "utils.h"

#include <QJsonArray>
#include <QJsonObject>

namespace Artist {

Controller::Controller( Model::Artist *artist ) : BaseController(), artist_( artist ), view_( new View() ) {}

Controller::~Controller() { view_->deleteLater(); }

QWidget *Controller::view() { return view_; }

void Controller::execute( IControllerHost *host ) {
  view_->releaseClicked.connect( [host]( Model::Release *release ) { host->activateController( new Release::Controller( release ) ); } );
  view_->masterClicked.connect( [host]( Model::Master *master ) { host->activateController( new Master::Controller( master ) ); } );
  host->discogs()->getArtist( artist_->discogsID, [this, host]( Model::Artist *artist ) {
    view_->setName( artist->title );
    view_->setBio( artist->bio );
    host->discogs()->getArtistReleases( artist_, [this]( const std::vector<Model::Releasable *> &releasables ) {
      auto f = overload(
          [this]( Model::Release *release ) {
            release->artist = artist_;
            view_->addRelease( release );
          },
          [this]( Model::Master *master ) {
            master->artist = artist_;
            view_->addRelease( master );
          } );
      std::for_each( releasables.begin(), releasables.end(), std::bind( &Model::Releasable::apply, std::placeholders::_1, f ) );
    } );
  } );
}

} // namespace Controller
