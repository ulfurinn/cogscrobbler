#ifndef DISCOGS_PROVIDER_H
#define DISCOGS_PROVIDER_H

#include <memory>

#include "discogs_action.h"
#include "discogs_callback.h"
#include <QObject>

namespace Discogs {

class Backend;
class Cache;

class Provider {
public:
  explicit Provider( Backend * );
  ~Provider();

  void artistSearch( const QString &, Callback::ArtistList );
  void getArtist( int, Callback::Artist );
  void getArtistReleases( Model::Artist *artist, Callback::ReleasableList );
  void getRelease( int, Callback::Release );
  void getMaster( Model::Artist *artist, int, Callback::Master );
  void getMasterVersions( int, Callback::MasterVersionList );
  void loadCollection( Callback::ReleasableList );
  void saveReleaseToCollection( int id );

signals:

public slots:

private:
  Backend *backend_;
  std::unique_ptr<Cache> cache_;
};

} // namespace Discogs

#endif // DISCOGS_PROVIDER_H
