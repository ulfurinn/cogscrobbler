#include "release_controller.h"
#include "discogs_provider.h"
#include "icontrollerhost.h"
#include "release_view.h"
#include "utils.h"
#include <QDateTime>
#include <QDebug>
#include <QJsonArray>
#include <QJsonObject>
#include <numeric>

namespace Release {

Controller::Controller( Model::Release *release ) : BaseController(), view_( new Release::View() ), release_( release ) {
  view_->trackSelectionChanged.connect( [this]( int i, bool selected ) { trackSelectionChanged( i, selected ); } );
  view_->trackDurationChanged.connect( [this]( int i, int duration ) { trackDurationChanged( i, duration ); } );
}

Controller::~Controller() { view_->deleteLater(); }

void Controller::trackSelectionChanged( int i, bool on ) { selected_[i] = on; }

void Controller::trackDurationChanged( int i, int duration ) { release_->tracks[i].duration = duration; }

void Controller::execute( IControllerHost *host ) {
  view_->scrobble.connect( [this, host]( ScrobbleTime st, QDateTime time ) {
    std::vector<Model::Track> scrobbleList;
    qDebug() << "collecting list";
    qDebug() << "total tracks" << release_->tracks.size();
    for ( unsigned i = 0; i < release_->tracks.size(); i++ ) {
      qDebug() << "selected" << i << "=" << selected_[i];
      qDebug() << "duration" << i << release_->tracks[i].duration;
      if ( selected_[i] && release_->tracks[i].duration > 30 ) {
        qDebug() << "adding to list";
        scrobbleList.emplace_back( release_->tracks[i] );
      }
    }
    qDebug() << scrobbleList.size() << "tracks selected for scrobble";
    if ( !scrobbleList.empty() )
      host->scrobble( scrobbleList, st, time.toTime_t() );
  } );

  view_->saveLocal.connect( [this, host]() { host->storage()->save( release_ ); } );
  view_->saveDiscogs.connect( [this, host]() { host->discogs()->saveReleaseToCollection( release_->discogsID ); } );

  host->discogs()->getRelease( release_->discogsID, [this]( Model::Release *release ) {
    release_->artist = release->artist;
    release_->title = release->title;
    release_->format = release->format;
    release_->year = release->year;
    release_->uri = release->uri;
    release_->tracks = release->tracks;
    selected_.clear();
    std::for_each( release->tracks.begin(), release->tracks.end(), [this]( Model::Track &t ) {
      t.releaseTitle = release_->title;
      if ( t.artist.isEmpty() ) {
        t.artist = release_->artist->title;
      }
      auto index = selected_.size();
      selected_[index] = t.duration > 30;
    } );
    view_->setArtist( release->artist->title );
    view_->setTitle( release->title );
    view_->setDate( release_->year );
    view_->setTracks( release->tracks );
  } );
}

QWidget *Controller::view() { return view_; }

} // namespace Release
