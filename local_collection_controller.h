#ifndef LOCALCOLLECTION_CONTROLLER_H
#define LOCALCOLLECTION_CONTROLLER_H

#include <functional>

#include "base_controller.h"
#include "istorage.h"
#include "models.h"

namespace LocalCollection {

class View;

class Controller : public BaseController {
public:
  explicit Controller( IStorage *storage );
  ~Controller();

  // BaseController interface
  virtual void execute( IControllerHost * ) override;
  virtual QWidget *view() override;

private:
  IStorage *storage_;
  View *view_;
  std::function<void( Model::Release * )> releaseAction_;
};

} // namespace LocalCollection

#endif // LOCALCOLLECTION_CONTROLLER_H
