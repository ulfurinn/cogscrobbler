#ifndef SIDEBARMODEL_H
#define SIDEBARMODEL_H

#include <QAbstractItemModel>

class SidebarItem;

class SidebarModel : public QAbstractItemModel {
  Q_OBJECT
public:
  explicit SidebarModel( QObject *parent = 0 );

signals:

public slots:

  // QAbstractItemModel interface
public:
  virtual QModelIndex index( int row, int column, const QModelIndex &parent ) const;
  virtual QModelIndex parent( const QModelIndex &child ) const;
  virtual int rowCount( const QModelIndex &parent ) const;
  virtual int columnCount( const QModelIndex &parent ) const;
  virtual QVariant data( const QModelIndex &index, int role ) const;

  void setRoot( SidebarItem * );
  SidebarItem *root();

  void addItem( SidebarItem *parent, SidebarItem *child );
  void activate( const QModelIndex & );

  void createInitialItems();

  SidebarItem *home();
  SidebarItem *searches();
  SidebarItem *localCollection();
  SidebarItem *discogsCollection();
  SidebarItem *settings();

protected:
  QModelIndex itemToIndex( SidebarItem * );

private:
  SidebarItem *root_;

  SidebarItem *home_, *searches_, *collections_, *settings_;
  SidebarItem *local_collection_, *discogs_collection_;
};

#endif // SIDEBARMODEL_H
