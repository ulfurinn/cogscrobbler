#ifndef MASTER_CONTROLLER_H
#define MASTER_CONTROLLER_H

#include <functional>

#include "base_controller.h"
#include "models.h"

namespace Discogs {
class Provider;
}

namespace Master {

class View;

class Controller : public BaseController {
public:
  explicit Controller( Model::Master *master );
  ~Controller();

  void trackSelectionChanged( int, bool );
  void trackDurationChanged( int, int );

  // BaseController interface
public:
  virtual void execute( IControllerHost * ) override;
  virtual QWidget *view() override;

private:
  Master::View *view_;
  Model::Master *master_;
  int currentVersion_;
  std::map<int, Model::MasterVersion *> versions_;
  std::map<int, std::vector<Model::Track>> tracks_;
  std::map<int, bool> selected_;
};

} // namespace Master

#endif // MASTER_CONTROLLER_H
