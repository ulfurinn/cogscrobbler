#ifndef DISCOGSCOLLECTION_CONTROLLER_H
#define DISCOGSCOLLECTION_CONTROLLER_H

#include <functional>

#include "base_controller.h"
#include "models.h"

namespace Discogs {
class Provider;
}

namespace DiscogsCollection {

class View;

class Controller : public BaseController {
public:
  explicit Controller( Discogs::Provider *discogs );
  ~Controller();

  void save( Model::Release *release );
  void setReleaseAction( std::function<void( Model::Release * )> );

  // BaseController interface
  virtual void execute( IControllerHost * ) override;
  virtual QWidget *view() override;

private:
  Discogs::Provider *discogs_;
  View *view_;
  std::function<void( Model::Release * )> releaseAction_;
};

} // namespace DiscogsCollection

#endif // DISCOGSCOLLECTION_CONTROLLER_H
