#ifndef DBMIGRATOR_H
#define DBMIGRATOR_H

class QSqlDatabase;

class DBMigrator {
public:
  DBMigrator();
  static void migrate( QSqlDatabase &db );
};

#endif // DBMIGRATOR_H
