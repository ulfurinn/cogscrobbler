#ifndef COGSCROBBLER_H
#define COGSCROBBLER_H

#include <functional>
#include <map>
#include <memory>

#include <QSqlDatabase>

#include "icontrollerhost.h"
#include "sidebarmodel.h"

class MainWindow;
namespace Artist {
class Controller;
}
namespace Release {
class Controller;
}
namespace Master {
class Controller;
}
namespace Discogs {
class Backend;
class Provider;
}
class LastFM;
namespace Settings {
class Controller;
}
namespace LocalCollection {
class Controller;
}
namespace DiscogsCollection {
class Controller;
}
namespace Model {
struct Master;
struct Release;
struct Track;
}

class Cogscrobbler : public IControllerHost {
public:
  explicit Cogscrobbler( MainWindow *main_window );
  ~Cogscrobbler();

private:
  MainWindow *main_window_;
  QSqlDatabase db_;

  std::unique_ptr<Discogs::Backend> discogsBackend_;
  std::unique_ptr<Discogs::Provider> discogsProvider_;
  std::unique_ptr<LastFM> lastfm_;
  std::unique_ptr<Settings::Controller> settings_;

  QWidget *current_displayed_widget_;
  SidebarModel sidebar_;
  std::unique_ptr<IStorage> storage_;
  std::unique_ptr<LocalCollection::Controller> localCollectionController_;
  std::unique_ptr<DiscogsCollection::Controller> discogsCollectionController_;

  std::map<int, std::function<BaseController *( const QString & )>> searchEngines_;

  void findDatabase();
  void setupMembers();
  void setupTree();
  void setupSettings();
  void setCurrentWidget( QWidget * );

private slots:

  void showHome();
  void requireConfiguration( bool );
  void sidebarActivate( const QModelIndex &current );
  void search();

  // IControllerHost interface
public:
  virtual void activateController( BaseController * ) override;
  virtual Discogs::Provider *discogs() override;
  virtual IStorage *storage() override;
  virtual void scrobble( const std::vector<Model::Track> &list, ScrobbleTime st, uint listen ) override;
};

#endif // COGSCROBBLER_H
