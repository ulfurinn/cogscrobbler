#ifndef SEARCHVIEW_H
#define SEARCHVIEW_H

#include "models.h"
#include <QWidget>
#include <boost/signals2.hpp>

namespace Ui {
class SearchView;
}

namespace Search {
using namespace boost::signals2;
class View : public QWidget {
  Q_OBJECT

public:
  explicit View( QWidget *parent = 0 );
  ~View();

  void setTerm( const QString &term );
  void addArtist( Model::Artist *artist );

  signal<void( Model::Artist * )> artistClicked;

private:
  Ui::SearchView *ui;
  int artistsCount_;
  QWidget *empty_;
};
}
#endif // SEARCHVIEW_H
