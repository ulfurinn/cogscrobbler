#include "track_widget.h"
#include "ui_track_widget.h"
#include "utils.h"

#include <QDebug>
#include <QMouseEvent>

TrackWidget::TrackWidget( QWidget *parent ) : QWidget( parent ), ui( new Ui::TrackWidget ) {
  ui->setupUi( this );
  ui->txtDuration->setVisible( false );
  connect( ui->checkBox, SIGNAL( toggled( bool ) ), this, SIGNAL( selected( bool ) ) );
  connect( ui->labDuration, &QClickableLabel::clicked, this, &TrackWidget::showDurationEditor );
  connect( ui->txtDuration, &QLineEdit::editingFinished, this, &TrackWidget::hideDurationEditor );
}

TrackWidget::~TrackWidget() { delete ui; }

void TrackWidget::setPosition( const QString &position ) { ui->labIndex->setText( position ); }

void TrackWidget::setTitle( const QString &title ) { ui->labTitle->setText( title ); }

void TrackWidget::setArtist( const QString &artist ) { ui->labArtist->setText( artist ); }

void TrackWidget::setDuration( int duration ) {
  duration_ = duration;
  int minute = duration / 60;
  int second = duration % 60;
  QString formatted = QStringLiteral( "%1:%2" ).arg( minute, 2, 10, QChar( '0' ) ).arg( second, 2, 10, QChar( '0' ) );
  ui->labDuration->setText( formatted );
  ui->txtDuration->setText( formatted );
  ui->checkBox->setChecked( duration > 30 );
  ui->checkBox->setEnabled( duration > 30 );
}

void TrackWidget::setDuration( const QString &duration ) { setDuration( parseDuration( duration ) ); }

void TrackWidget::showDurationEditor() {
  ui->labDuration->setVisible( false );
  ui->txtDuration->setVisible( true );
  ui->txtDuration->setFocus();
}

void TrackWidget::hideDurationEditor() {
  setDuration( ui->txtDuration->text() );
  emit durationEdited( duration_ );
  emit selected( duration_ > 30 );
  ui->labDuration->setVisible( true );
  ui->txtDuration->setVisible( false );
}
