#include "release_button.h"
#include "ui_release_button.h"
#include <QDebug>
#include <QMouseEvent>

ReleaseButton::ReleaseButton( QWidget *parent ) : QFrame( parent ), ui( new Ui::ReleaseButton ) {
  ui->setupUi( this );
  ui->labArtist->setVisible( false );
}

ReleaseButton::~ReleaseButton() { delete ui; }

void ReleaseButton::showArtist( bool visible ) { ui->labArtist->setVisible( visible ); }

void ReleaseButton::setRelease( Model::Master *master ) {
  if ( master->artist != nullptr ) {
    ui->labArtist->setText( master->artist->title );
  }
  ui->labTitle->setText( master->title );
  ui->labFormat->setText( master->format );
  ui->labYear->setText( master->year == 0 ? "" : QStringLiteral( "%1" ).arg( master->year ) );
  ui->labMaster->setText( "[master]" );
}

void ReleaseButton::setRelease( Model::Release *release ) {
  if ( release->artist != nullptr ) {
    ui->labArtist->setText( release->artist->title );
  }
  ui->labTitle->setText( release->title );
  ui->labFormat->setText( release->format );
  ui->labYear->setText( release->year == 0 ? "" : QStringLiteral( "%1" ).arg( release->year ) );
  ui->labMaster->setText( "" );
}

void ReleaseButton::mousePressEvent( QMouseEvent *me ) {
  if ( me->buttons() ^ me->button() ) {
    QFrame::mousePressEvent( me );
    return;
  }

  if ( me->button() ) {
    emit( clicked() );
    me->accept();
    return;
  }

  QFrame::mousePressEvent( me );
}
