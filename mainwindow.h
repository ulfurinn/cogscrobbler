#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class QTextEdit;

namespace Ui {
class MainWindow;
}

class Cogscrobbler;

class MainWindow : public QMainWindow {
  Q_OBJECT

  friend Cogscrobbler;

public:
  explicit MainWindow( QWidget *parent = 0 );
  ~MainWindow();

  QTextEdit *loggerOutput();
  Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
