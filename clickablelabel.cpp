#include "clickablelabel.h"

QClickableLabel::QClickableLabel( QWidget *parent ) : QLabel( parent ) {}

void QClickableLabel::mousePressEvent( QMouseEvent *me ) {
  if ( me->buttons() ^ me->button() ) {
    QLabel::mousePressEvent( me );
    return;
  }

  if ( me->button() ) {
    emit( clicked() );
    me->accept();
    return;
  }

  QLabel::mousePressEvent( me );
}
