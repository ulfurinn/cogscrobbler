#include "utils.h"
#include <QCryptographicHash>
#include <QRegExp>
#include <QStringList>
#include <QtGlobal>

const char *charset = "abcdefghijklmnopqrstuvwxyz";

QChar randChar() { return QChar( charset[qrand() % strlen( charset )] ); }

QString randomString( int length ) {
  QString r( length, ' ' );
  std::generate( r.begin(), r.end(), &randChar );
  return r;
}

QString md5( const QString &string ) {
  QCryptographicHash hash( QCryptographicHash::Md5 );
  QByteArray hsh = hash.hash( string.toUtf8(), QCryptographicHash::Md5 );
  return QString( hsh.toHex() );
}

int parseDuration( const QString &duration ) {
  if ( duration.isEmpty() )
    return 0;

  QRegExp hms( "^(\\d+):(\\d{1,2}):(\\d{1,2})$" ), ms( "^(\\d+):(\\d{1,2})$" ), s( "^(\\d+)$" );
  int hour( 0 ), minute( 0 ), second( 0 );

  if ( hms.exactMatch( duration ) ) {
    auto groups = hms.capturedTexts();
    hour = groups[1].toInt();
    minute = groups[2].toInt();
    second = groups[3].toInt();
  } else if ( ms.exactMatch( duration ) ) {
    auto groups = ms.capturedTexts();
    minute = groups[1].toInt();
    second = groups[2].toInt();
  } else if ( s.exactMatch( duration ) ) {
    auto groups = s.capturedTexts();
    second = groups[1].toInt();
  }
  return hour * 3600 + minute * 60 + second;
}

QString stripDiscogsIndex( QString name ) { return name.replace( QRegExp( " \\(\\d+\\)$" ), "" ); }
