#ifndef DISCOGSCOLLECTION_DISCOGS_COLLECTION_VIEW_H
#define DISCOGSCOLLECTION_DISCOGS_COLLECTION_VIEW_H

#include "models.h"
#include <QWidget>
#include <boost/signals2.hpp>

namespace DiscogsCollection {

namespace Ui {
class View;
}

using namespace boost::signals2;
class View : public QWidget {
  Q_OBJECT

public:
  explicit View( QWidget *parent = 0 );
  ~View();

  void addRelease( Model::Release * );

  signal<void( Model::Release * )> releaseClicked;

private:
  Ui::View *ui;
  QWidget *empty_;

  void createDummy();
};

} // namespace DiscogsCollection
#endif // DISCOGSCOLLECTION_DISCOGS_COLLECTION_VIEW_H
