#ifndef ENUM_H
#define ENUM_H

#include <QDebug>

enum class LastFMState { UNKNOWN, UNAUTHORIZED, PENDING, AUTHORIZED };
enum class DiscogsState { UNKNOWN, UNAUTHORIZED, PENDING, AUTHORIZED };

enum class ScrobbleTime { STARTED_AT, FINISHED_AT };

QDebug &operator<<( QDebug &d, LastFMState state );
QDebug &operator<<( QDebug &d, DiscogsState state );

#endif // ENUM_H
