#include "release_view.h"
#include "track_widget.h"
#include "ui_release_view.h"
#include <QDebug>
#include <QMenu>

namespace Release {

View::View( QWidget *parent ) : QWidget( parent ), ui( new Ui::View ), empty_( nullptr ) {
  ui->setupUi( this );
  ui->datetime->setDateTime( QDateTime::currentDateTime() );
  connect( ui->checkBox, &QCheckBox::stateChanged, [this]( int state ) {
    ui->checkBox->setTristate( false );
    if ( state == Qt::CheckState::Checked )
      emit massSelect( true );
    if ( state == Qt::CheckState::Unchecked )
      emit massSelect( false );
  } );

  auto menu = new QMenu( this );
  saveLocal_ = new QAction( "Save to local collection", this );
  saveDiscogs_ = new QAction( "Save to Discogs", this );
  menu->addAction( saveLocal_ );
  menu->addAction( saveDiscogs_ );
  ui->btnSave->setMenu( menu );

  auto scrobbleMenu = new QMenu( this );
  auto scrobbleFinishedNow = new QAction( "...as just finished", this );
  auto scrobbleStartedNow = new QAction( "...as just started", this );
  auto scrobbleFinishedAt = new QAction( "...as finished at:", this );
  auto scrobbleStartedAt = new QAction( "...as started at:", this );
  scrobbleMenu->addAction( scrobbleFinishedNow );
  scrobbleMenu->addAction( scrobbleStartedNow );
  scrobbleMenu->addAction( scrobbleFinishedAt );
  scrobbleMenu->addAction( scrobbleStartedAt );
  ui->btnScrobble->setMenu( scrobbleMenu );

  connect( saveLocal_, &QAction::triggered, [this]() { saveLocal(); } );
  connect( saveDiscogs_, &QAction::triggered, [this]() { saveDiscogs(); } );

  connect( scrobbleFinishedNow, &QAction::triggered, [this]() { scrobble( ScrobbleTime::FINISHED_AT, QDateTime::currentDateTimeUtc() ); } );
  connect( scrobbleStartedNow, &QAction::triggered, [this]() { scrobble( ScrobbleTime::STARTED_AT, QDateTime::currentDateTimeUtc() ); } );
  connect( scrobbleFinishedAt, &QAction::triggered, [this]() { scrobble( ScrobbleTime::FINISHED_AT, ui->datetime->dateTime().toUTC() ); } );
  connect( scrobbleStartedAt, &QAction::triggered, [this]() { scrobble( ScrobbleTime::STARTED_AT, ui->datetime->dateTime().toUTC() ); } );
}

View::~View() { delete ui; }

void View::setArtist( const QString &v ) {
  artist_ = v;
  ui->labArtist->setText( v );
}

void View::setTitle( const QString &v ) { ui->labTitle->setText( v ); }

void View::setDate( int v ) {
  if ( v > 0 ) {
    ui->labDate->setText( QStringLiteral( "%1" ).arg( v ) );
  } else {
    ui->labDate->clear();
  }
}

void View::setTracks( std::vector<Model::Track> tracks ) {
  int i( 0 );
  createDummy();
  for ( auto track : tracks ) {
    auto widget = new TrackWidget( this );
    widget->setPosition( track.position );
    widget->setTitle( track.title );
    widget->setDuration( track.duration );
    if ( track.artist != artist_ )
      widget->setArtist( track.artist );
    else
      widget->setArtist( "" );
    connect( widget, &TrackWidget::selected, [this, i]( bool on ) { emit trackSelectionChanged( i, on ); } );
    connect( widget, &TrackWidget::durationEdited, [this, i]( int duration ) { emit trackDurationChanged( i, duration ); } );
    ui->scrollAreaWidgetContents->layout()->addWidget( widget );
    i++;
  }
  ui->scrollAreaWidgetContents->layout()->addWidget( empty_ );
}

void View::createDummy() {
  if ( empty_ == nullptr ) {
    empty_ = new QWidget( ui->scrollAreaWidgetContents );
    empty_->setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Expanding );
  } else
    ui->scrollAreaWidgetContents->layout()->removeWidget( empty_ );
}

} // namespace Release
