#include <QCoreApplication>
#include <QDebug>
#include <QDir>
#include <QFileInfo>

#include "cogscrobbler.h"
#include "dbmigrator.h"
#include "discogs_backend.h"
#include "discogs_collection_controller.h"
#include "discogs_provider.h"
#include "http_request.h"
#include "lastfm.h"
#include "local_collection_controller.h"
#include "local_collection_storage.h"
#include "mainwindow.h"
#include "release_controller.h"
#include "search_controller.h"
#include "settings_controller.h"
#include "sidebaritem.h"
#if QT_VERSION >= QT_VERSION_CHECK( 5, 6, 0 )
#include "ui_mainwindow-5.6.h"
#else
#include "ui_mainwindow.h"
#endif
#include "utils.h"

Cogscrobbler::Cogscrobbler( MainWindow *main_window )
    : main_window_( main_window ), db_( QSqlDatabase::addDatabase( "QSQLITE", "cog" ) ), discogsBackend_( new Discogs::Backend() ),
      discogsProvider_( new Discogs::Provider( discogsBackend_.get() ) ), current_displayed_widget_( nullptr ), storage_( new LocalCollection::Storage( db_ ) ),
      localCollectionController_( new LocalCollection::Controller( storage_.get() ) ),
      discogsCollectionController_( new DiscogsCollection::Controller( discogsProvider_.get() ) ) {

  qDebug() << "<a href=\"http://cogscrobbler.ulfurinn.net/\">cogscrobbler 0.4</a>";

  findDatabase();
  DBMigrator::migrate( db_ );

  setupMembers();
  setupTree();
  setupSettings();

  main_window_->ui->cmbSearchEngine->addItem( "Discogs artist search", QVariant() );
  main_window_->ui->cmbSearchEngine->addItem( "Discogs release ID", QVariant() );

  searchEngines_[0] = [this]( const QString &term ) -> BaseController * { return new Search::Controller( term ); };
  searchEngines_[1] = [this]( const QString &term ) -> BaseController * {
    bool ok = false;
    int id = term.toInt( &ok, 10 );
    if ( !ok ) {
      return nullptr;
    }
    auto release = new Model::Release();
    release->discogsID = id;
    return new Release::Controller( release );
  };

  settings_->read();
  localCollectionController_->execute( this );
  lastfm_->verify();
  discogsBackend_->verify();
}

Cogscrobbler::~Cogscrobbler() {}

void Cogscrobbler::findDatabase() {
  QString file = "/.cog-scrobbler.db";
  QString path;
  typedef std::vector<QFileInfo> fiList;

  fiList candidates;
  candidates.push_back( QCoreApplication::applicationDirPath() + file );
  candidates.push_back( QDir::currentPath() + file );
  candidates.push_back( QDir::homePath() + file );

  qDebug() << "Looking for the application database";

  fiList::iterator existing = std::find_if( candidates.begin(), candidates.end(), []( QFileInfo fi ) {
    qDebug() << "Checking" << fi.filePath();
    return fi.exists();
  } );

  if ( existing == candidates.end() ) {
    path = ( existing - 1 )->filePath();
  } else {
    path = existing->filePath();
  }

  qDebug() << "Database location is" << path;

  db_.setDatabaseName( path );
  if ( !db_.open() ) {
    qCritical() << "Failed to open database";
  }
}

void Cogscrobbler::setupMembers() {

  main_window_->ui->panelMessage->setVisible( false );
  current_displayed_widget_ = main_window_->ui->webView;

  HTTPRequest::manager = new QNetworkAccessManager();

  settings_ = std::unique_ptr<Settings::Controller>( new Settings::Controller( &db_ ) );
  lastfm_ = std::unique_ptr<LastFM>( new LastFM() );

  main_window_->ui->sidetree->setModel( &sidebar_ );
  main_window_->ui->sidetree->setFocusPolicy( Qt::NoFocus );

  QObject::connect( main_window_->ui->txtSearchTerm, &QLineEdit::returnPressed, [this]() { search(); } );
  QObject::connect( main_window_->ui->btnSearch, &QPushButton::clicked, [this]() { search(); } );
}

void Cogscrobbler::setupTree() {

  QObject::connect( main_window_->ui->sidetree, &QTreeView::clicked, [this]( const QModelIndex &index ) { sidebarActivate( index ); } );

  sidebar_.createInitialItems();

  sidebar_.home()->setAction( [this]() { showHome(); } );
  sidebar_.settings()->setAction( [this]() { setCurrentWidget( settings_->view() ); } );
  sidebar_.localCollection()->setAction( [this]() { setCurrentWidget( localCollectionController_->view() ); } );
  sidebar_.discogsCollection()->setAction( [this]() { setCurrentWidget( discogsCollectionController_->view() ); } );
}

void Cogscrobbler::setupSettings() {
  settings_->lastfmCredentials.connect( [this]( const QString &key ) { lastfm_->setCredentials( key ); } );
  settings_->discogsCredentials.connect( [this]( const QString &token, const QString &secret ) { discogsBackend_->setCredentials( token, secret ); } );
  settings_->discogsReadCollection.connect( [this]( int id ) { discogsBackend_->setReadCollection( id ); } );
  settings_->discogsWriteCollection.connect( [this]( int id ) { discogsBackend_->setWriteCollection( id ); } );

  discogsBackend_->unauthorized.connect( [this]() { settings_->discogsUnauthorized(); } );
  discogsBackend_->authSecondStep.connect( [this]( const QString &token ) { settings_->discogsAuthSecondStep( token ); } );
  discogsBackend_->authorized.connect(
      [this]( const QString &token, const QString &secret, const QString &username ) { settings_->discogsAuthorized( token, secret, username ); } );
  discogsBackend_->collections.connect( [this]( std::vector<Model::DiscogsCollection> collections ) { settings_->discogsCollections( collections ); } );

  lastfm_->unauthorized.connect( [this]() { settings_->lastfmUnauthorized(); } );
  lastfm_->authSecondStep.connect( [this]( const QString &token ) { settings_->lastfmAuthSecondStep( token ); } );
  lastfm_->authorized.connect( [this]( const QString &key, const QString &username ) { settings_->lastfmAuthorized( key, username ); } );

  settings_->requiresConfiguration.connect( [this]( bool require ) { requireConfiguration( require ); } );

  settings_->initiateDiscogsLogin.connect( [this]() { discogsBackend_->initiateAuth(); } );
  settings_->completeDiscogsLogin.connect( [this]( const QString &key ) { discogsBackend_->completeAuth( key ); } );

  settings_->initiateLastfmLogin.connect( [this]() { lastfm_->initiateAuth(); } );
  settings_->completeLastfmLogin.connect( [this]() { lastfm_->completeAuth(); } );
}

void Cogscrobbler::setCurrentWidget( QWidget *new_widget ) {
  if ( current_displayed_widget_ == new_widget ) {
    return;
  }
  if ( current_displayed_widget_ != nullptr ) {
    current_displayed_widget_->setVisible( false );
    main_window_->ui->mainContainer->layout()->removeWidget( current_displayed_widget_ );
  }
  current_displayed_widget_ = new_widget;
  main_window_->ui->mainContainer->layout()->addWidget( new_widget );
  new_widget->setVisible( true );
}

void Cogscrobbler::showHome() { setCurrentWidget( main_window_->ui->webView ); }

void Cogscrobbler::requireConfiguration( bool v ) {
  main_window_->ui->panelMessage->setVisible( v );
  if ( !v )
    discogsCollectionController_->execute( this ); //  TODO: rewrite
}

void Cogscrobbler::sidebarActivate( const QModelIndex &current ) { sidebar_.activate( current ); }

void Cogscrobbler::search() {
  auto term = main_window_->ui->txtSearchTerm->text();
  main_window_->ui->txtSearchTerm->setText( "" );
  auto search = searchEngines_[main_window_->ui->cmbSearchEngine->currentIndex()]( term );
  if ( search == nullptr )
    return;
  auto searchItem = new SidebarItem( term );
  searchItem->setAction( [this, search]() { setCurrentWidget( search->view() ); } );
  sidebar_.addItem( sidebar_.searches(), searchItem );
  setCurrentWidget( search->view() );
  search->execute( this );
}

void Cogscrobbler::scrobble( const std::vector<Model::Track> &list, ScrobbleTime st, uint listen ) {
  std::vector<LastFM::ScrobbleItem> batch;
  int start = ( st == ScrobbleTime::STARTED_AT )
                  ? listen
                  : listen - std::accumulate( list.begin(), list.end(), 0, []( int sum, const Model::Track &track ) { return sum + track.duration; } );
  std::transform( list.begin(), list.end(), std::back_inserter( batch ), [&start]( const Model::Track &track ) {
    LastFM::ScrobbleItem item;
    item.artist = stripDiscogsIndex( track.artist );
    item.release = track.releaseTitle;
    item.track = track.title;
    item.duration = track.duration;
    item.startTimestamp = start;
    start += track.duration;
    return item;
  } );
  lastfm_->scrobble( batch );
}

void Cogscrobbler::activateController( BaseController *controller ) {
  controller->execute( this );
  setCurrentWidget( controller->view() );
}

Discogs::Provider *Cogscrobbler::discogs() { return discogsProvider_.get(); }

IStorage *Cogscrobbler::storage() { return storage_.get(); }
