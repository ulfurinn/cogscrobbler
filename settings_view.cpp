#include "settings_view.h"
#include "secret.h"
#include "ui_settings_view.h"

namespace Settings {

View::View( QWidget *parent ) : QWidget( parent ), ui( new Ui::settings_view ) {
  ui->setupUi( this );

  connect( ui->btnBeginLastfmAuth, &QPushButton::clicked, [this]() { initiateLastfmLogin(); } );
  connect( ui->btnCompleteLastfmAuth, &QPushButton::clicked, [this]() { completeLastfmLogin(); } );
  connect( ui->btnResetLastfmAuth, &QPushButton::clicked, [this]() { resetLastfmLogin(); } );
  connect( ui->btnBeginDiscogsAuth, &QPushButton::clicked, [this]() {
    ui->btnBeginDiscogsAuth->setEnabled( false );
    initiateDiscogsLogin();
  } );
  connect( ui->btnCompleteDiscogsAuth, &QPushButton::clicked, [this]() {
    ui->btnCompleteDiscogsAuth->setEnabled( false );
    ui->btnResetDiscogsAuth->setEnabled( false );
    ui->txtDiscogsVerification->setEnabled( false );
    completeDiscogsLogin( ui->txtDiscogsVerification->text() );
  } );
  connect( ui->btnResetDiscogsAuth, &QPushButton::clicked, [this]() { resetDiscogsLogin(); } );

  ui->btnBeginDiscogsAuth->setEnabled( false );
  ui->btnCompleteDiscogsAuth->setEnabled( false );
  ui->btnResetDiscogsAuth->setEnabled( false );
  ui->labDiscogsAuthState->setText( "" );
  ui->txtDiscogsVerification->setVisible( false );

  ui->btnBeginLastfmAuth->setEnabled( false );
  ui->btnCompleteLastfmAuth->setEnabled( false );
  ui->btnResetLastfmAuth->setEnabled( false );
  ui->labLastfmAuthState->setText( "" );

  ui->groupCollections->setEnabled( false );
  ui->cmbCollectionRead->setEnabled( false );
  ui->cmbCollectionWrite->setEnabled( false );
}

View::~View() { delete ui; }

void View::lastfmSessionUser( QString name ) { ui->labLastfmAuthState->setText( QStringLiteral( "Logged in as <b>%1</b>" ).arg( name ) ); }

void View::discogsUnauthorized() {
  ui->btnBeginDiscogsAuth->setEnabled( true );
  ui->btnCompleteDiscogsAuth->setEnabled( false );
  ui->btnResetDiscogsAuth->setEnabled( false );
  ui->labDiscogsAuthState->setText( "" );
  ui->txtDiscogsVerification->setVisible( false );
  ui->groupCollections->setEnabled( false );
}

void View::discogsAuthSecondStep( const QString &token ) {
  ui->btnBeginDiscogsAuth->setEnabled( false );
  ui->btnCompleteDiscogsAuth->setEnabled( true );
  ui->btnResetDiscogsAuth->setEnabled( true );
  QString url = QStringLiteral( "http://discogs.com/oauth/authorize?oauth_token=%1" ).arg( token );
  ui->labDiscogsAuthState->setText( QStringLiteral( "Go to <a href=\"%1\">Discogs</a> and enter the code you "
                                                    "receive there:" )
                                        .arg( url ) );
  ui->txtDiscogsVerification->setVisible( true );
  ui->groupCollections->setEnabled( false );
}

void View::discogsAuthorized( const QString &username ) {
  ui->btnBeginDiscogsAuth->setEnabled( false );
  ui->btnCompleteDiscogsAuth->setEnabled( false );
  ui->btnResetDiscogsAuth->setEnabled( true );
  ui->labDiscogsAuthState->setText( QStringLiteral( "You are logged in as <b>%1</b>." ).arg( username ) );
  ui->txtDiscogsVerification->setVisible( false );
  ui->groupCollections->setEnabled( true );
}

void View::discogsCollections( std::vector<Model::DiscogsCollection> collections ) {
  for ( auto collection : collections ) {
    ui->cmbCollectionRead->addItem( collection.name );
    readCollections_.push_back( collection.discogsID );
    if ( collection.discogsID == readCollection_ )
      ui->cmbCollectionRead->setCurrentIndex( ui->cmbCollectionRead->count() - 1 );
    if ( collection.discogsID == 0 )
      continue;
    ui->cmbCollectionWrite->addItem( collection.name );
    writeCollections_.push_back( collection.discogsID );
    if ( collection.discogsID == writeCollection_ )
      ui->cmbCollectionWrite->setCurrentIndex( ui->cmbCollectionWrite->count() - 1 );
  }
}

void View::lastfmUnauthorized() {
  ui->btnBeginLastfmAuth->setEnabled( true );
  ui->btnCompleteLastfmAuth->setEnabled( false );
  ui->btnResetLastfmAuth->setEnabled( false );
  ui->labLastfmAuthState->setText( "" );
}

void View::lastfmAuthSecondStep( const QString &token ) {
  ui->btnBeginLastfmAuth->setEnabled( false );
  ui->btnCompleteLastfmAuth->setEnabled( true );
  ui->btnResetLastfmAuth->setEnabled( true );
  QString url = QStringLiteral( "http://www.last.fm/api/auth/?api_key=%1&token=%2" ).arg( LASTFM_KEY ).arg( token );
  ui->labLastfmAuthState->setText( QStringLiteral( "Go to <a href=\"%1\">last.fm</a> to complete the process." ).arg( url ) );
}

void View::lastfmAuthorized( const QString &username ) {
  ui->btnBeginLastfmAuth->setEnabled( false );
  ui->btnCompleteLastfmAuth->setEnabled( false );
  ui->btnResetLastfmAuth->setEnabled( true );
  ui->labLastfmAuthState->setText( QStringLiteral( "You are logged in as <b>%1</b>." ).arg( username ) );
}

void View::setReadCollection( int id ) { readCollection_ = id; }

void View::setWriteCollection( int id ) { writeCollection_ = id; }
}
