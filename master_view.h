#ifndef MASTER_MASTER_VIEW_H
#define MASTER_MASTER_VIEW_H

#include "enum.h"
#include "models.h"
#include <QWidget>
#include <boost/signals2.hpp>

class QAction;

namespace Master {

namespace Ui {
class View;
}

using namespace boost::signals2;
class View : public QWidget {
  Q_OBJECT

public:
  explicit View( QWidget *parent = 0 );
  ~View();

  void setMainReleaseID( int );
  void setArtist( const QString & );
  void setTitle( const QString & );
  void setDate( int );
  void setTracks( std::vector<Model::Track> );
  void setVersions( const std::map<int, Model::MasterVersion *> & );

  signal<void( ScrobbleTime, QDateTime )> scrobble;
  signal<void( int, bool )> trackSelectionChanged;
  signal<void( int, int )> trackDurationChanged;
  signal<void( bool )> massSelect;
  signal<void( int )> releaseSelected;
  signal<void()> saveLocal;
  signal<void()> saveDiscogs;

private:
  Ui::View *ui;
  QWidget *empty_;
  QString artist_;
  int mainReleaseID_;
  std::vector<Model::MasterVersion *> versions_;

  QAction *saveLocal_;
  QAction *saveDiscogs_;

  void createDummy();
};

} // namespace Master
#endif // MASTER_MASTER_VIEW_H
