#include "cogscrobbler.h"
#include "mainwindow.h"
#include "texteditlogger.h"
#include <QApplication>
#include <QStyleFactory>
#include <QTime>

int main( int argc, char *argv[] ) {
  qsrand( QTime::currentTime().msec() );

  QApplication a( argc, argv );
  QApplication::setStyle( QStyleFactory::create( "Fusion" ) );

  //    QPalette p;
  //    p = a.palette();
  //    p.setColor(QPalette::Window, QColor(53,53,53));
  //    p.setColor(QPalette::Button, QColor(53,53,53));
  //    p.setColor(QPalette::Highlight, QColor(142,45,197));
  //    p.setColor(QPalette::ButtonText, QColor(255,255,255));
  //    p.setColor(QPalette::Text, QColor(255,255,255));
  //    p.setColor(QPalette::WindowText, QColor(255,255,255));
  //    a.setPalette(p);

  MainWindow w;

  TextEditLogger logger( w.loggerOutput() );
  qInstallMessageHandler( TextEditLogger::handler );

  Cogscrobbler c( &w );
  w.show();

  return a.exec();
}
