#ifndef SECRET_H
#define SECRET_H

extern const char *DISCOGS_CONSUMER_KEY;
extern const char *DISCOGS_CONSUMER_SECRET;

extern const char *LASTFM_KEY;
extern const char *LASTFM_SECRET;

#endif // SECRET_H
