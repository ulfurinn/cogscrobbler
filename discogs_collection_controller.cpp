#include "discogs_collection_controller.h"
#include "discogs_collection_view.h"
#include "discogs_provider.h"
#include "overload.h"

#include <QDebug>

namespace DiscogsCollection {

Controller::Controller( Discogs::Provider *discogs ) : BaseController(), discogs_( discogs ), view_( new View() ) {
  view_->releaseClicked.connect( [this]( Model::Release *release ) {
    if ( releaseAction_ )
      releaseAction_( release );
  } );
}

Controller::~Controller() { view_->deleteLater(); }

void Controller::save( Model::Release * ) {}

void Controller::setReleaseAction( std::function<void( Model::Release * )> f ) { releaseAction_ = f; }

void Controller::execute( IControllerHost * ) {
  qDebug() << "loading discogs collections";
  discogs_->loadCollection( [this]( const std::vector<Model::Releasable *> &releasables ) {
    auto f = overload( [this]( Model::Release *release ) { view_->addRelease( release ); }, [this]( Model::Master * ) {} );
    std::for_each( releasables.begin(), releasables.end(), std::bind( &Model::Releasable::apply, std::placeholders::_1, f ) );
  } );
}

QWidget *Controller::view() { return view_; }

} // namespace DiscogsCollection
