#ifndef CLICKABLELABEL_H
#define CLICKABLELABEL_H

#include <QLabel>
#include <QMouseEvent>

class QClickableLabel : public QLabel {
  Q_OBJECT
public:
  explicit QClickableLabel( QWidget *parent = 0 );

signals:
  void clicked();

public slots:

  // QWidget interface
protected:
  virtual void mousePressEvent( QMouseEvent * );
};

#endif // CLICKABLELABEL_H
