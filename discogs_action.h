#ifndef DISCOGS_ACTION_H
#define DISCOGS_ACTION_H

namespace Discogs {
class Action {
public:
  virtual void run() = 0;
};
}

#endif // DISCOGS_ACTION_H
