#include <QDateTime>
#include <QJsonArray>
#include <QJsonObject>
#include <numeric>

#include "discogs_provider.h"
#include "icontrollerhost.h"
#include "master_controller.h"
#include "master_view.h"
#include "utils.h"

#include <QDebug>

namespace Master {

Controller::Controller( Model::Master *master ) : BaseController(), view_( new Master::View() ), master_( master ), currentVersion_( master->mainReleaseID ) {
  view_->trackSelectionChanged.connect( [this]( int i, bool selected ) { trackSelectionChanged( i, selected ); } );
  view_->trackDurationChanged.connect( [this]( int i, int duration ) { trackDurationChanged( i, duration ); } );
}

Controller::~Controller() { view_->deleteLater(); }

void Controller::trackSelectionChanged( int i, bool on ) { selected_[i] = on; }
void Controller::trackDurationChanged( int i, int duration ) { tracks_[currentVersion_][i].duration = duration; }

QWidget *Controller::view() { return view_; }

void Controller::execute( IControllerHost *host ) {

  view_->releaseSelected.connect( [this, host]( int id ) {
    currentVersion_ = id;
    selected_.clear();
    if ( tracks_.find( currentVersion_ ) != tracks_.end() ) {
      for_each_indexed( tracks_[currentVersion_].begin(), tracks_[currentVersion_].end(),
                        [this]( int i, Model::Track &t ) { selected_[i] = t.duration >= 30; } );
      view_->setTracks( tracks_[currentVersion_] );
      return;
    }
    host->discogs()->getRelease( id, [this, id]( Model::Release *release ) {
      qDebug() << "received release" << release->discogsID << "with" << release->tracks.size() << "tracks";
      for_each_indexed( release->tracks.begin(), release->tracks.end(), [this]( int i, Model::Track &t ) { selected_[i] = t.duration >= 30; } );
      tracks_[id] = release->tracks;
      view_->setTracks( release->tracks );
    } );
  } );

  view_->scrobble.connect( [this, host]( ScrobbleTime st, QDateTime time ) {
    qDebug() << "collecting list";
    std::vector<Model::Track> scrobbleList;
    qDebug() << "current version" << currentVersion_;
    qDebug() << "total tracks" << tracks_[currentVersion_].size();
    for_each_indexed( tracks_[currentVersion_].begin(), tracks_[currentVersion_].end(), [this, &scrobbleList]( int i, const Model::Track &track ) {
      qDebug() << "selected" << i << "=" << selected_[i];
      qDebug() << "duration" << i << track.duration;
      if ( selected_[i] && track.duration > 30 ) {
        qDebug() << "adding to list";
        scrobbleList.emplace_back( track );
      }
    } );
    qDebug() << scrobbleList.size() << "tracks selected for scrobble";
    if ( !scrobbleList.empty() )
      host->scrobble( scrobbleList, st, time.toTime_t() );
  } );

  view_->saveLocal.connect( [this, host]() {
    auto release = new Model::Release();
    release->discogsID = currentVersion_;
    release->artist = master_->artist;
    release->format = versions_[currentVersion_]->format;
    release->title = master_->title;
    release->tracks = tracks_[currentVersion_];
    host->storage()->save( release );
  } );
  view_->saveDiscogs.connect( [this, host]() { host->discogs()->saveReleaseToCollection( versions_[currentVersion_]->discogsID ); } );

  view_->setMainReleaseID( master_->mainReleaseID );
  view_->setArtist( master_->artist->title );
  view_->setTitle( master_->title );
  view_->setDate( master_->year );

  host->discogs()->getMaster( master_->artist, master_->discogsID, [this]( Model::Master *master ) {
    selected_.clear();
    std::for_each( master->tracks.begin(), master->tracks.end(), [this]( Model::Track &track ) {
      track.releaseTitle = master_->title;
      if ( track.artist.isEmpty() ) {
        track.artist = master_->artist->title;
      }
      auto index = selected_.size();
      selected_[index] = track.duration > 30;
    } );
    tracks_[currentVersion_] = master->tracks;
    view_->setTracks( master->tracks );
  } );

  host->discogs()->getMasterVersions( master_->discogsID, [this]( const std::map<int, Model::MasterVersion *> &versions ) {
    versions_ = versions;
    view_->setVersions( versions );
  } );
}

} // namespace Master
