#include "sidebarmodel.h"
#include "sidebaritem.h"
#include <QDebug>
#include <QSize>

SidebarModel::SidebarModel( QObject *parent ) : QAbstractItemModel( parent ) { root_ = new SidebarItem( "ROOT" ); }

QModelIndex SidebarModel::index( int row, int column, const QModelIndex &parent ) const {
  if ( !hasIndex( row, column, parent ) )
    return QModelIndex();
  SidebarItem *parent_item;
  if ( !parent.isValid() )
    parent_item = root_;
  else
    parent_item = static_cast<SidebarItem *>( parent.internalPointer() );
  return createIndex( row, column, parent_item->child( row ) );
}

QModelIndex SidebarModel::parent( const QModelIndex &child ) const {
  if ( !child.isValid() )
    return QModelIndex();
  SidebarItem *child_item = static_cast<SidebarItem *>( child.internalPointer() );
  SidebarItem *parent_item = child_item->parent();
  if ( parent_item == nullptr || parent_item == root_ )
    return QModelIndex();
  return createIndex( parent_item->row(), 0, parent_item );
}

int SidebarModel::rowCount( const QModelIndex &parent ) const {
  if ( parent.column() > 0 )
    return 0;
  SidebarItem *item;
  if ( !parent.isValid() )
    item = root_;
  else
    item = static_cast<SidebarItem *>( parent.internalPointer() );
  return item->size();
}

int SidebarModel::columnCount( const QModelIndex & ) const { return 1; }

QVariant SidebarModel::data( const QModelIndex &index, int role ) const {
  if ( !index.isValid() )
    return QVariant();
  SidebarItem *item = static_cast<SidebarItem *>( index.internalPointer() );
  switch ( role ) {
  case Qt::DisplayRole:
    return item->title();
  case Qt::SizeHintRole:
    return QSize( 0, 25 );
  default:
    return QVariant();
  }
}

void SidebarModel::setRoot( SidebarItem *item ) {
  root_ = item;
  emit dataChanged( QModelIndex(), QModelIndex() );
}

SidebarItem *SidebarModel::root() { return root_; }

void SidebarModel::addItem( SidebarItem *parent, SidebarItem *child ) {
  auto index = itemToIndex( parent );
  auto n = parent->size();
  beginInsertRows( index, n, n );
  parent->append( child );
  endInsertRows();
}

void SidebarModel::activate( const QModelIndex &index ) {
  if ( !index.isValid() )
    return;
  SidebarItem *item = static_cast<SidebarItem *>( index.internalPointer() );
  item->activate();
}

void SidebarModel::createInitialItems() {
  home_ = new SidebarItem( "Home" );
  addItem( root(), home_ );

  searches_ = new SidebarItem( "Search results" );
  addItem( root(), searches_ );

  collections_ = new SidebarItem( "Collections" );
  addItem( root(), collections_ );

  local_collection_ = new SidebarItem( "Local collection" );
  addItem( collections_, local_collection_ );

  discogs_collection_ = new SidebarItem( "Discogs collection" );
  addItem( collections_, discogs_collection_ );

  settings_ = new SidebarItem( "Settings" );
  addItem( root(), settings_ );
}

SidebarItem *SidebarModel::home() { return home_; }

SidebarItem *SidebarModel::searches() { return searches_; }

SidebarItem *SidebarModel::localCollection() { return local_collection_; }

SidebarItem *SidebarModel::discogsCollection() { return discogs_collection_; }

SidebarItem *SidebarModel::settings() { return settings_; }

QModelIndex SidebarModel::itemToIndex( SidebarItem *item ) {
  if ( item == root_ )
    return QModelIndex();
  return createIndex( item->row(), 0, item );
}
