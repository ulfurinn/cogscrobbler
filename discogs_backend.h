#ifndef DISCOGS_H
#define DISCOGS_H

#include "discogs_action.h"
#include "discogs_callback.h"
#include "models.h"
#include <boost/signals2.hpp>

namespace Discogs {
using namespace boost::signals2;
class Backend {
public:
  explicit Backend();

  void verify();
  void fetchCollections();
  QString token();
  QString secret();
  QString verification();
  const QString &username();
  int readCollection();
  int writeCollection();

  void authFirstStepPassed( const QString &token, const QString &secret );
  void verified( const QString &username );

  Action *artistSearch( const QString &term, Callback::Generic callback );
  Action *getArtist( int id, Callback::Generic callback );
  Action *getArtistReleases( int id, Callback::Generic callback );
  Action *getRelease( int id, Callback::Generic callback );
  Action *getMaster( int id, Callback::Generic callback );
  Action *getMasterVersions( int id, Callback::Generic callback );
  Action *loadCollection( Callback::Generic callback );
  Action *saveReleaseToCollection( int id );

  signal<void()> unauthorized;
  signal<void( const QString &token )> authSecondStep;
  signal<void( const QString &token, const QString &secret, const QString &username )> authorized;
  signal<void( std::vector<Model::DiscogsCollection> )> collections;

  void setCredentials( const QString &token, const QString &secret );
  void setReadCollection( int );
  void setWriteCollection( int );

  void initiateAuth();
  void completeAuth( const QString &key );

private:
  QString token_;
  QString secret_;
  QString verification_;
  QString username_;
  int readCollection_, writeCollection_;
};
}
#endif // DISCOGS_H
