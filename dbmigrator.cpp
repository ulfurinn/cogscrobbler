#include "dbmigrator.h"
#include <QDebug>
#include <QSqlQuery>
#include <QVariant>

class schema {
private:
  static const int SCHEMA_VERSION = 5;

  int getVersion( QSqlQuery &q ) {
    q.exec( "SELECT COALESCE(MAX(version), 0) FROM schema_version" );
    if ( q.next() ) {
      return q.value( 0 ).toInt();
    } else {
      return 0;
    }
  }

  void setVersion( int version, QSqlQuery &q ) {
    q.exec( "DELETE FROM schema_version" );
    q.prepare( "INSERT INTO schema_version (version) VALUES (:version)" );
    q.bindValue( ":version", version );
    q.exec();
  }

  template <int version> void upgrade( QSqlQuery & );

  template <int version> void installSchema( int current_version, QSqlQuery &q ) {
    if ( current_version == version )
      return;
    installSchema<version - 1>( current_version, q );
    qDebug() << "Migrating to" << version;
    upgrade<version>( q );
    setVersion( version, q );
  }

public:
  void install( QSqlQuery &q ) {
    int version = getVersion( q );
    qDebug() << "Database schema" << version;
    installSchema<SCHEMA_VERSION>( getVersion( q ), q );
  }
};

template <> void schema::installSchema<0>( int, QSqlQuery & ) {
  //  end recursion
}

template <> void schema::upgrade<1>( QSqlQuery &q ) {
  q.exec( "CREATE TABLE settings (name TEXT, value TEXT, CONSTRAINT pk_settings "
          "PRIMARY KEY (name) ON CONFLICT REPLACE)" );
  q.exec( "CREATE TABLE schema_version (version INT, CONSTRAINT "
          "pk_schema_version PRIMARY KEY (version) ON CONFLICT REPLACE)" );
}

template <> void schema::upgrade<2>( QSqlQuery &q ) {
  q.exec( "CREATE TABLE artists (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT)" );
  q.exec( "CREATE TABLE releases (id INTEGER PRIMARY KEY AUTOINCREMENT, "
          "artist_id INTEGER, year INTEGER, name TEXT, format TEXT)" );
  q.exec( "CREATE TABLE tracks (id INTEGER PRIMARY KEY AUTOINCREMENT, "
          "release_id INTEGER, track_artist NAME, duration INTEGER, name TEXT, "
          "position TEXT)" );
}

template <> void schema::upgrade<3>( QSqlQuery &q ) {
  q.exec( "CREATE TABLE artists_meta (artist_id INTEGER, key TEXT, value TEXT)" );
  q.exec( "CREATE TABLE releases_meta (artist_id INTEGER, key TEXT, value TEXT)" );
  q.exec( "CREATE TABLE tracks_meta (artist_id INTEGER, key TEXT, value TEXT)" );
}

template <> void schema::upgrade<4>( QSqlQuery &q ) {
  //  `started` is a iso8601 datetime
  q.exec( "CREATE TABLE submission_queue (id INTEGER PRIMARY KEY AUTOINCREMENT, "
          "artist TEXT, album TEXT, track TEXT, started TEXT, duration "
          "INTEGER)" );
}

//  upgrade to 0.4 -- resetting auth state
template <> void schema::upgrade<5>( QSqlQuery &q ) {
  q.exec( "DELETE FROM settings" );

  q.exec( "ALTER TABLE releases_meta RENAME TO tmp" );
  q.exec( "CREATE TABLE releases_meta (release_id INTEGER, key TEXT, value TEXT)" );
  q.exec( "INSERT INTO releases_meta (release_id, key, value) SELECT artist_id, key, value FROM tmp" );
  q.exec( "DROP TABLE tmp" );

  q.exec( "ALTER TABLE tracks_meta RENAME TO tmp" );
  q.exec( "CREATE TABLE tracks_meta (track_id INTEGER, key TEXT, value TEXT)" );
  q.exec( "INSERT INTO tracks_meta (track_id, key, value) SELECT artist_id, key, value FROM tmp" );
  q.exec( "DROP TABLE tmp" );
}

DBMigrator::DBMigrator() {}

void DBMigrator::migrate( QSqlDatabase &db ) {
  QSqlQuery q( db );
  schema().install( q );
}
