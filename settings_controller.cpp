#include "settings_controller.h"
#include "settings_view.h"

#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>

const QString Settings::Controller::LASTFM_SESSION_KEY = "last-fm.session-key";
const QString Settings::Controller::DISCOGS_SESSION_TOKEN = "discogs.session-token";
const QString Settings::Controller::DISCOGS_SESSION_SECRET = "discogs.session-secret";
const QString Settings::Controller::LAST_USED_SEARCH_ENGINE = "last-used-search-engine";
const QString Settings::Controller::DISCOGS_READ_COLLECTION = "discogs.read-collection";
const QString Settings::Controller::DISCOGS_WRITE_COLLECTION = "discogs.write-collection";

namespace Settings {

Controller::Controller( QSqlDatabase *db )
    : BaseController(), db_( db ), discogsState_( DiscogsState::UNKNOWN ), lastfmState_( LastFMState::UNKNOWN ), view_( new View() ) {
  view_->initiateDiscogsLogin.connect( [this]() { initiateDiscogsLogin(); } );
  view_->completeDiscogsLogin.connect( [this]( const QString &key ) { completeDiscogsLogin( key ); } );
  view_->initiateLastfmLogin.connect( [this]() { initiateLastfmLogin(); } );
  view_->completeLastfmLogin.connect( [this]() { completeLastfmLogin(); } );
}

Controller::~Controller() { view_->deleteLater(); }

void Controller::read() {
  QSqlQuery q( *db_ );
  std::vector<QString> noise_settings;
  bool ok = q.exec( "SELECT * FROM settings" );
  if ( !ok ) {
    qCritical() << q.lastError();
    return;
  }
  QSqlRecord r = q.record();

  bool seenReadCollection( false ), seenWriteCollection( false );
  QString discogsToken, discogsSecret;

  while ( q.next() ) {
    QString name = q.value( r.indexOf( "name" ) ).toString();
    QString value = q.value( r.indexOf( "value" ) ).toString();
    if ( name == LAST_USED_SEARCH_ENGINE ) {
      emit searchEngine( value );
    } else if ( name == LASTFM_SESSION_KEY && value != "" ) {
      emit lastfmCredentials( value );
    } else if ( name == DISCOGS_SESSION_TOKEN && value != "" ) {
      discogsToken = value;
    } else if ( name == DISCOGS_SESSION_SECRET && value != "" ) {
      discogsSecret = value;
    } else if ( name == DISCOGS_READ_COLLECTION && value != "" ) {
      emit discogsReadCollection( value.toInt() );
      seenReadCollection = true;
    } else if ( name == DISCOGS_WRITE_COLLECTION && value != "" ) {
      emit discogsWriteCollection( value.toInt() );
      seenWriteCollection = true;
    } else {
      noise_settings.push_back( name );
    }
  }

  if ( !discogsToken.isEmpty() && !discogsSecret.isEmpty() ) {
    emit discogsCredentials( discogsToken, discogsSecret );
  }
  if ( !seenReadCollection )
    emit discogsReadCollection( 0 );
  if ( !seenWriteCollection )
    emit discogsWriteCollection( 1 );

  QSqlQuery clear( *db_ );
  clear.prepare( "DELETE FROM settings WHERE name = :name" );
  for ( auto name : noise_settings ) {
    clear.bindValue( ":name", name );
    clear.exec();
  }
}

void Controller::discogsUnauthorized() {
  discogsState_ = DiscogsState::UNAUTHORIZED;
  setValue( DISCOGS_SESSION_TOKEN, "" );
  setValue( DISCOGS_SESSION_SECRET, "" );
  view_->discogsUnauthorized();
  checkValidity();
}

void Controller::discogsAuthSecondStep( const QString &token ) {
  discogsState_ = DiscogsState::PENDING;
  view_->discogsAuthSecondStep( token );
  checkValidity();
}

void Controller::discogsAuthorized( const QString &token, const QString &secret, const QString &username ) {
  discogsState_ = DiscogsState::AUTHORIZED;
  setValue( DISCOGS_SESSION_TOKEN, token );
  setValue( DISCOGS_SESSION_SECRET, secret );
  view_->discogsAuthorized( username );
  checkValidity();
}

void Controller::setDiscogsReadCollection( int id ) { setValue( DISCOGS_READ_COLLECTION, QString::number( id ) ); }

void Controller::setDiscogsWriteCollection( int id ) { setValue( DISCOGS_WRITE_COLLECTION, QString::number( id ) ); }

void Controller::lastfmUnauthorized() {
  lastfmState_ = LastFMState::UNAUTHORIZED;
  setValue( LASTFM_SESSION_KEY, "" );
  view_->lastfmUnauthorized();
  checkValidity();
}

void Controller::lastfmAuthSecondStep( const QString &token ) {
  lastfmState_ = LastFMState::PENDING;
  view_->lastfmAuthSecondStep( token );
  checkValidity();
}

void Controller::lastfmAuthorized( const QString &key, const QString &username ) {
  lastfmState_ = LastFMState::AUTHORIZED;
  setValue( LASTFM_SESSION_KEY, key );
  view_->lastfmAuthorized( username );
  checkValidity();
}

void Controller::discogsCollections( std::vector<Model::DiscogsCollection> ) {}

void Controller::setValue( const QString &name, const QString &value ) {
  QSqlQuery q( *db_ );
  q.prepare( "INSERT INTO settings (name, value) VALUES (:name, :value)" );
  q.bindValue( ":name", name );
  q.bindValue( ":value", value );
  q.exec();
}

void Controller::checkValidity() {
  if ( discogsState_ == DiscogsState::UNKNOWN || lastfmState_ == LastFMState::UNKNOWN )
    return;
  auto discogsNoauth = discogsState_ != DiscogsState::AUTHORIZED;
  auto lastfmNoauth = lastfmState_ != LastFMState::AUTHORIZED;
  requiresConfiguration( discogsNoauth || lastfmNoauth );
}

void Controller::execute( IControllerHost * ) {}

QWidget *Controller::view() { return view_; }
}
