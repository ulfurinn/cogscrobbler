#include "http_request.h"

HTTPRequest::HTTPRequest() {}

void HTTPRequest::execute() {
  reply_ = startRequest();
  if ( reply_ == nullptr ) {
    emit aborted();
    return;
  }
  connect( reply_, SIGNAL( error( QNetworkReply::NetworkError ) ), this, SLOT( error( QNetworkReply::NetworkError ) ) );
  connect( reply_, SIGNAL( finished() ), this, SLOT( finished() ) );
}

void HTTPRequest::error( QNetworkReply::NetworkError errorCode ) { handleError( errorCode ); }

void HTTPRequest::finished() {
  handleResponse();
  reply_->deleteLater();
  deleteLater();
}

QNetworkAccessManager *HTTPRequest::manager;
