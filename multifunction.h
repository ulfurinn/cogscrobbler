#ifndef MULTIFUNCTION_H
#define MULTIFUNCTION_H

#include <functional>

template <class... Ts> struct multifunction;

template <class T1, class... Ts> struct multifunction<T1, Ts...> : std::function<T1>, multifunction<Ts...> {
  template <typename F> multifunction( F impl ) : std::function<T1>( impl ), multifunction<Ts...>( impl ) {}
  using std::function<T1>::operator();
  using multifunction<Ts...>::operator();
};

template <class T> struct multifunction<T> : std::function<T> {
  template <typename F> multifunction( F impl ) : std::function<T>( impl ) {}
  using std::function<T>::operator();
};

#endif // MULTIFUNCTION_H
