#-------------------------------------------------
#
# Project created by QtCreator 2014-10-18T00:38:41
#
#-------------------------------------------------

QT       += core gui network sql
CONFIG   += c++11
!win32 { QMAKE_CXXFLAGS += -std=c++1y }

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

equals(QT_MAJOR_VERSION, 5) {
    greaterThan(QT_MINOR_VERSION, 5) {
        QT += webenginewidgets
    } else {
        QT += webkitwidgets
    }
}

TARGET = cogscrobbler
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    cogscrobbler.cpp \
    settings_view.cpp \
    texteditlogger.cpp \
    dbmigrator.cpp \
    enum.cpp \
    lastfm.cpp \
    http_request.cpp \
    utils.cpp \
    sidebarmodel.cpp \
    sidebaritem.cpp \
    searchview.cpp \
    artistbutton.cpp \
    clickablegraphicsview.cpp \
    secret.cpp \
    release_button.cpp \
    release_controller.cpp \
    release_view.cpp \
    base_controller.cpp \
    track_widget.cpp \
    master_controller.cpp \
    master_view.cpp \
    local_collection_storage.cpp \
    local_collection_controller.cpp \
    local_collection_view.cpp \
    discogs_collection_controller.cpp \
    discogs_collection_view.cpp \
    clickablelabel.cpp \
    settings_controller.cpp \
    discogs_provider.cpp \
    discogs_backend.cpp \
    models.cpp \
    search_controller.cpp \
    artist_controller.cpp \
    artist_view.cpp

greaterThan(QT_MINOR_VERSION, 5) {
    SOURCES += external_browser_page.cpp
}

HEADERS  += mainwindow.h \
    settings_view.h \
    texteditlogger.h \
    dbmigrator.h \
    enum.h \
    lastfm.h \
    http_request.h \
    common.h \
    secret.h \
    utils.h \
    sidebarmodel.h \
    sidebaritem.h \
    searchview.h \
    artistbutton.h \
    clickablegraphicsview.h \
    models.h \
    release_button.h \
    release_controller.h \
    release_view.h \
    base_controller.h \
    track_widget.h \
    master_controller.h \
    master_view.h \
    cogscrobbler.h \
    local_collection_storage.h \
    local_collection_controller.h \
    local_collection_view.h \
    discogs_collection_controller.h \
    discogs_collection_view.h \
    clickablelabel.h \
    settings_controller.h \
    discogs_callback.h \
    discogs_provider.h \
    discogs_backend.h \
    discogs_action.h \
    overload.h \
    multifunction.h \
    search_controller.h \
    artist_controller.h \
    artist_view.h \
    icontrollerhost.h \
    istorage.h

greaterThan(QT_MINOR_VERSION, 5) {
    HEADERS += external_browser_page.h
}

FORMS    += settings_view.ui \
    searchview.ui \
    artistbutton.ui \
    view_artist.ui \
    release_button.ui \
    release_view.ui \
    track_widget.ui \
    master_view.ui \
    local_collection_view.ui \
    discogs_collection_view.ui

greaterThan(QT_MINOR_VERSION, 5) {
    FORMS += mainwindow-5.6.ui
} else {
    FORMS += mainwindow.ui
}

win32 {
    INCLUDEPATH += c:\opt\boost_1_60_0
    RC_FILE = res\app.rc
}

ICON = main.icns

#RESOURCES += \
#    cogscrobbler.qrc

target.path = /usr/bin
INSTALLS += target
