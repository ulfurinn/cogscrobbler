#include "local_collection_storage.h"
#include "utils.h"

#include <QSqlQuery>
#include <QSqlRecord>
#include <QVariant>

namespace LocalCollection {
Storage::Storage( QSqlDatabase &db ) : db_( db ) {}

std::vector<Model::Release *> Storage::fetch() {
  std::map<int, Model::Artist *> artists;
  std::vector<Model::Release *> releases;
  {
    QSqlQuery q( db_ ), metaq( db_ );
    q.exec( "SELECT * FROM artists" );
    metaq.prepare( "SELECT value FROM artists_meta WHERE artist_id = :artist_id AND key = :key" );
    while ( q.next() ) {
      auto artist = new Model::Artist();
      artist->title = q.value( "name" ).toString();
      int id = q.value( "id" ).toInt();
      metaq.bindValue( ":artist_id", id );
      metaq.bindValue( ":key", "discogs2.artist.id" );
      metaq.exec();
      if ( metaq.next() ) {
        artist->discogsID = metaq.value( "value" ).toString().toInt();
      }
      artists[id] = artist;
    }
  }
  {
    QSqlQuery q( db_ ), subq( db_ ), metaq( db_ );
    q.exec( "SELECT releases.* FROM releases JOIN artists ON releases.artist_id = artists.id ORDER BY artists.name" );
    subq.prepare( "SELECT * FROM tracks WHERE release_id = :release_id ORDER BY id" );
    metaq.prepare( "SELECT value FROM releases_meta WHERE release_id = :release_id AND key = :key" );
    while ( q.next() ) {
      auto release = new Model::Release();
      release->title = q.value( "name" ).toString();
      release->format = q.value( "format" ).toString();
      release->year = q.value( "year" ).toInt();
      release->artist = artists[q.value( "artist_id" ).toInt()];
      int id = q.value( "id" ).toInt();

      metaq.bindValue( ":release_id", id );
      metaq.bindValue( ":key", "discogs2.release.id" );
      metaq.exec();
      if ( metaq.next() ) {
        release->discogsID = metaq.value( "value" ).toString().toInt();
      } else {
        continue;
      }

      subq.bindValue( ":release_id", id );
      subq.exec();
      while ( subq.next() ) {
        Model::Track track;
        track.title = subq.value( "name" ).toString();
        track.position = subq.value( "position" ).toString();
        track.duration = subq.value( "duration" ).toInt();
        track.releaseTitle = release->title;
        QString trackArtist = subq.value( "track_artist" ).toString();
        if ( trackArtist.isEmpty() )
          trackArtist = release->artist->title;
        track.artist = trackArtist;
        release->tracks.emplace_back( track );
      }
      releases.emplace_back( release );
    }
  }
  return releases;
}

Model::Release *Storage::save( Model::Release *release ) {
  db_.transaction();
  int localArtistID( 0 );
  QSqlQuery findArtistByDiscogsID( db_ ), findArtistByName( db_ );
  findArtistByDiscogsID.prepare( "SELECT artist_id FROM artists_meta WHERE key = 'discogs2.artist.id' AND value = :id" );
  findArtistByDiscogsID.bindValue( ":id", release->artist->discogsID );
  findArtistByDiscogsID.exec();
  if ( findArtistByDiscogsID.next() ) {
    localArtistID = findArtistByDiscogsID.value( "artist_id" ).toString().toInt();
  } else {
    findArtistByName.prepare( "SELECT id FROM artists WHERE name = :name" );
    findArtistByName.bindValue( ":name", stripDiscogsIndex( release->artist->title ) );
    findArtistByName.exec();
    if ( findArtistByName.next() ) {
      localArtistID = findArtistByName.value( "id" ).toString().toInt();
    }
  }

  if ( localArtistID == 0 ) {
    QSqlQuery createArtist( db_ );
    createArtist.prepare( "INSERT INTO artists (name) VALUES (:name)" );
    createArtist.bindValue( ":name", stripDiscogsIndex( release->artist->title ) );
    createArtist.exec();
    localArtistID = createArtist.lastInsertId().toInt();
    createArtist.prepare( "INSERT INTO artists_meta (artist_id, key, value) VALUES (:id, 'discogs2.artist.id', :value)" );
    createArtist.bindValue( ":id", localArtistID );
    createArtist.bindValue( ":value", release->artist->discogsID );
    createArtist.exec();
  }

  QSqlQuery insertRelease( db_ ), releaseMeta( db_ ), insertTrack( db_ );
  insertRelease.prepare( "INSERT INTO releases (artist_id, name, format, year) VALUES (:artist_id, :name, :format, :year)" );
  insertRelease.bindValue( ":artist_id", localArtistID );
  insertRelease.bindValue( ":name", release->title );
  insertRelease.bindValue( ":format", release->format );
  insertRelease.bindValue( ":year", release->year );
  insertRelease.exec();
  int localReleaseID = insertRelease.lastInsertId().toInt();
  releaseMeta.prepare( "INSERT INTO releases_meta (release_id, key, value) VALUES (:release_id, 'discogs2.release.id', :value)" );
  releaseMeta.bindValue( ":release_id", localReleaseID );
  releaseMeta.bindValue( ":value", release->discogsID );
  releaseMeta.exec();

  insertTrack.prepare( "INSERT INTO tracks (release_id, name, position, duration, track_artist) VALUES (:release_id, :name, :position, :duration, :artist)" );
  for ( auto track : release->tracks ) {
    insertTrack.bindValue( ":release_id", localReleaseID );
    insertTrack.bindValue( ":name", track.title );
    insertTrack.bindValue( ":position", track.position );
    insertTrack.bindValue( ":duration", track.duration );
    insertTrack.bindValue( ":artist", track.artist );
    insertTrack.exec();
  }
  db_.commit();
  release->tracks.clear();
  return release;
}
}
