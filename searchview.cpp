#include "searchview.h"
#include "artistbutton.h"
#include "ui_searchview.h"
#include <QDebug>
#include <QPushButton>

namespace Search {
View::View( QWidget *parent ) : QWidget( parent ), ui( new Ui::SearchView ), artistsCount_( 0 ), empty_( nullptr ) { ui->setupUi( this ); }

View::~View() { delete ui; }

void View::setTerm( const QString &term ) { ui->labSearchTerm->setText( QStringLiteral( "Searching for: %1" ).arg( term ) ); }

void View::addArtist( Model::Artist *artist ) {
  auto layout = dynamic_cast<QGridLayout *>( ui->scrollAreaWidgetContents->layout() );
  if ( empty_ == nullptr ) {
    empty_ = new QWidget( ui->cntResults );
    empty_->setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Expanding );
  } else
    layout->removeWidget( empty_ );
  auto btn = new ArtistButton( ui->cntResults );
  btn->setText( artist->title );
  connect( btn, &ArtistButton::clicked, [this, artist]() { emit artistClicked( artist ); } );
  layout->addWidget( btn, artistsCount_ / 3, artistsCount_ % 3, Qt::AlignTop );
  layout->addWidget( empty_, artistsCount_ / 3 + 1, 0 );
  artistsCount_++;
}
}
