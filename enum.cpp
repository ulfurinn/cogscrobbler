#include "enum.h"

QDebug &operator<<( QDebug &d, LastFMState state ) {
  switch ( state ) {
  case LastFMState::UNAUTHORIZED:
    d << "UNAUTHORIZED";
    break;
  case LastFMState::PENDING:
    d << "PENDING";
    break;
  case LastFMState::AUTHORIZED:
    d << "AUTHORIZED";
    break;
  case LastFMState::UNKNOWN:
    d << "*unknown*";
    break;
  }
  return d;
}

QDebug &operator<<( QDebug &d, DiscogsState state ) {
  switch ( state ) {
  case DiscogsState::UNAUTHORIZED:
    d << "UNAUTHORIZED";
    break;
  case DiscogsState::PENDING:
    d << "PENDING";
    break;
  case DiscogsState::AUTHORIZED:
    d << "AUTHORIZED";
    break;
  case DiscogsState::UNKNOWN:
    d << "*unknown*";
    break;
  }
  return d;
}
