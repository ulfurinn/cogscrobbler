#include "local_collection_controller.h"
#include "icontrollerhost.h"
#include "local_collection_view.h"
#include "release_controller.h"

#include <QDebug>

namespace LocalCollection {

Controller::Controller( IStorage *storage ) : BaseController(), storage_( storage ), view_( new View() ) {}

Controller::~Controller() { view_->deleteLater(); }

void Controller::execute( IControllerHost *host ) {
  view_->releaseClicked.connect( [this, host]( Model::Release *release ) {
    auto controller = new Release::Controller( release );
    controller->setStorage( storage_ );
    host->activateController( controller );
  } );
  auto releases = storage_->fetch();
  std::for_each( releases.begin(), releases.end(), std::bind( &View::addRelease, view_, std::placeholders::_1 ) );
}

QWidget *Controller::view() { return view_; }

} // namespace LocalCollection
