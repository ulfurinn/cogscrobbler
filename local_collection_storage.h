#ifndef LOCALCOLLECTION_H
#define LOCALCOLLECTION_H

#include "istorage.h"
#include "models.h"

#include <QObject>
#include <QSqlDatabase>

namespace LocalCollection {
class Storage : public IStorage {
public:
  explicit Storage( QSqlDatabase &db );

  std::vector<Model::Release *> fetch();
  Model::Release *save( Model::Release * );

signals:

public slots:

private:
  QSqlDatabase &db_;
};
}

#endif // LOCALCOLLECTION_H
