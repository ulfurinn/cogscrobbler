#ifndef CLICKABLEGRAPHICSVIEW_H
#define CLICKABLEGRAPHICSVIEW_H

#include <QGraphicsView>
#include <QMouseEvent>

class ClickableGraphicsView : public QGraphicsView {
  Q_OBJECT
public:
  explicit ClickableGraphicsView( QWidget *parent = 0 );

signals:
  void clicked();

public slots:

  // QWidget interface
protected:
  virtual void mousePressEvent( QMouseEvent * );
};

#endif // CLICKABLEGRAPHICSVIEW_H
