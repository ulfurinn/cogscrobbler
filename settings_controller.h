#ifndef SETTINGS_H
#define SETTINGS_H

#include "base_controller.h"
#include "enum.h"
#include "models.h"
#include <QSqlDatabase>

#include <boost/signals2/signal.hpp>

namespace Settings {

using namespace boost::signals2;

class View;

class Controller : public BaseController {
public:
  explicit Controller( QSqlDatabase *db );
  ~Controller();
  void read();

  signal<void( const QString & )> searchEngine;
  signal<void( const QString & )> lastfmCredentials;
  signal<void( const QString &, const QString & )> discogsCredentials;
  signal<void( int )> discogsReadCollection;
  signal<void( int )> discogsWriteCollection;

  signal<void( bool )> requiresConfiguration;

  signal<void()> initiateDiscogsLogin;
  signal<void( const QString & )> completeDiscogsLogin;

  signal<void()> initiateLastfmLogin;
  signal<void()> completeLastfmLogin;

  void discogsUnauthorized();
  void discogsAuthSecondStep( const QString &token );
  void discogsAuthorized( const QString &token, const QString &secret, const QString &username );

  void setDiscogsReadCollection( int );
  void setDiscogsWriteCollection( int );

  void lastfmUnauthorized();
  void lastfmAuthSecondStep( const QString &token );
  void lastfmAuthorized( const QString &key, const QString &username );

  void discogsCollections( std::vector<Model::DiscogsCollection> );

private:
  QSqlDatabase *db_;

  DiscogsState discogsState_;
  LastFMState lastfmState_;

  void setValue( const QString &, const QString & );
  void checkValidity();

  static const QString LASTFM_SESSION_KEY;
  static const QString DISCOGS_SESSION_TOKEN;
  static const QString DISCOGS_SESSION_SECRET;
  static const QString LAST_USED_SEARCH_ENGINE;
  static const QString DISCOGS_READ_COLLECTION;
  static const QString DISCOGS_WRITE_COLLECTION;

  // BaseController interface
public:
  virtual void execute( IControllerHost * ) override;
  virtual QWidget *view() override;

private:
  View *view_;
};
}

#endif // SETTINGS_H
