#include "artistbutton.h"
#include "ui_artistbutton.h"
#include <QDebug>

ArtistButton::ArtistButton( QWidget *parent ) : QFrame( parent ), ui( new Ui::ArtistButton ) {
  ui->setupUi( this );
  connect( ui->img, SIGNAL( clicked() ), this, SIGNAL( clicked() ) );
}

ArtistButton::~ArtistButton() { delete ui; }

void ArtistButton::setText( const QString &title ) { ui->lab->setText( title ); }

void ArtistButton::setImageUrl( const QString & ) {}

void ArtistButton::mousePressEvent( QMouseEvent *me ) {
  if ( me->buttons() ^ me->button() ) {
    QFrame::mousePressEvent( me );
    return;
  }

  if ( me->button() ) {
    emit( clicked() );
    me->accept();
    return;
  }

  QFrame::mousePressEvent( me );
}
