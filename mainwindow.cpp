#include "mainwindow.h"

#if QT_VERSION >= QT_VERSION_CHECK( 5, 6, 0 )
#include "external_browser_page.h"
#include "ui_mainwindow-5.6.h"
#else
#include "ui_mainwindow.h"
#endif

#include <QDesktopServices>

MainWindow::MainWindow( QWidget *parent ) : QMainWindow( parent ), ui( new Ui::MainWindow ) {
  ui->setupUi( this );
#if QT_VERSION >= QT_VERSION_CHECK( 5, 6, 0 )
  ui->webView->setPage( new ExternalBrowserPage() );
  ui->webView->setUrl( QUrl( QStringLiteral( "http://cogscrobbler.ulfurinn.net/appres/0.4.0-alpha3/" ) ) );
#else
  ui->webView->page()->setLinkDelegationPolicy( QWebPage::DelegateExternalLinks );
  connect( ui->webView->page(), &QWebPage::linkClicked, []( const QUrl &url ) { QDesktopServices::openUrl( url ); } );
#endif
}

MainWindow::~MainWindow() { delete ui; }

QTextEdit *MainWindow::loggerOutput() { return ui->loggerOutput; }
