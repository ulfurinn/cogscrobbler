#include "texteditlogger.h"
#include <QTextEdit>

TextEditLogger *TextEditLogger::instance_;

TextEditLogger::TextEditLogger( QTextEdit *l ) : edit_( l ) { instance_ = this; }

void TextEditLogger::handler( QtMsgType type, const QMessageLogContext &context, const QString &message ) { instance_->log( type, context, message ); }

void TextEditLogger::log( QtMsgType type, const QMessageLogContext &, const QString &message ) {
  QString tag;
  QString formatted;
  switch ( type ) {
  case QtDebugMsg:
#if ( QT_VERSION >= QT_VERSION_CHECK( 5, 5, 0 ) )
  case QtInfoMsg:
#endif
    tag = "INFO";
    break;
  case QtWarningMsg:
    tag = "WARN";
    break;
  case QtCriticalMsg:
    tag = "CRIT";
    break;
  case QtFatalMsg:
    tag = "FAIL";
    break;
  }
  formatted = QStringLiteral( "<span style='color: gray'>[%1]</span> %2" ).arg( tag ).arg( message );
  edit_->append( formatted );
}
