#include "external_browser_page.h"
#include <QDesktopServices>

ExternalBrowserPage::ExternalBrowserPage( QObject *parent ) : QWebEnginePage( parent ) {}

bool ExternalBrowserPage::acceptNavigationRequest( const QUrl &url, NavigationType type, bool isMainFrame ) {
  if ( type == QWebEnginePage::NavigationTypeLinkClicked ) {
    QDesktopServices::openUrl( url );
    return false;
  }
  return QWebEnginePage::acceptNavigationRequest( url, type, isMainFrame );
}
